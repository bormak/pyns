#include "buffer.hpp"
#include <iostream>

// double Buffer::pduSz = 1.0;
int Buffer::header = 0; // fragmentation, packing header

Buffer::Buffer(int nodeId, int dest) : id(nodeId), _dest(dest) {
  rx = del = 0;
  sz = _data = 0;
}


// executed every tStep

void Buffer::input(double time, double data, int dest) {
  int burst = 0;
  if (!data)
    burst = _data;
      
  if (!dest)
    dest = _dest;
  
  if (burst)
    add(SDU(burst, time, id, dest));
  // a data burst is treated as SDU, delay calculated for the whole burst !!!
  // might be better to use mean throughput than delay for nrt services
}



void Buffer::remove(int data, double t) {
  // data given in number of frames
  int rx, tx; // received and transmitted pdu per sdu
  iterator sdu = begin();

  while (data > 0 && sdu != end()) {   // data - header
   
    tx = data > sdu->rem ? sdu->rem : data;
    
    // if (tx) {
    rx = send(tx);   // rx can be less than tx for derived send() 
    data -= tx;     // data -= tx + header
    sdu->rem -= rx;
    sz -= rx;  // buffer size
    assert(sz >= 0);
 
    
    if (!sdu->rem) {
      storeDelay(t - sdu->t, sdu->sz);
      sdu = erase(sdu);
    
    }
    else
      sdu++;
  }
  
}

void Buffer::content() {
   iterator sdu = begin();
   while (sdu != end()) {
     std::cout << "remaining " << sdu->rem << " from " << sdu->t << "\n";
     sdu++;
   }
}
  

object Buffer::repr() {
  object msg = "data %d size in sdu %d  size in pdu %d \n" % make_tuple(_data, size(), sz);
  return msg;
}

////////////////// Lbuffer /////////////////////////////////////

Lbuffer::Lbuffer(int nodeId, dict opt, int dest) : Buffer(nodeId, dest) {
  drop = load = 0;
  maxDelay = _d(opt["maxDelay"]);
  cap = _i(opt["cap"]);
  stream = _b(opt["stream"]);
  hist = opt["hist"]; // histogram for each service
  histSz = len(hist);
  tBin = _d(opt["tBin"]);
  // these variables not static as they are service specific
}




void Lbuffer::add(SDU sdu) {

  load += sdu.sz;
  if (sz <= cap - sdu.sz) 
    Buffer::add(sdu);
  else
    drop += sdu.sz; 
}

// double Lbuffer::getData() {

//   if (!fc)  
//     return _data; // data is input rate
//   else {   
//     double rc = cap - sz;  // remaining capacity of the buffer
//     _data -= rc;   // remaining data
//     return rc;
//   }
// }
    
    
 
  
	
void Lbuffer::storeDelay(double dt, int nPDU) {
  if (histSz) {
    int n = (int) floor(dt/tBin);
    if (n >= histSz)
      n = histSz - 1;
    hist[n] += nPDU;
  }
  Buffer::storeDelay(dt,nPDU);  // buffer specific delay
}



double Lbuffer::check(double t) {
  iterator sdu = begin();
  double delay = 0, dt;
  while (sdu != end()) {
    // int nFr = (int) ceil(sdu->sz/frSz);
    dt = t - sdu->t + .005; // to avoid 0 delay
    if (dt >= maxDelay) {
      sz -= sdu->rem; // !!
      storeDelay(dt,sdu->sz);  // delay statistic shouldn't be decreased by drop
      drop += sdu->sz;
      sdu = erase(sdu); // check 
    } 
    else {
      delay += dt*sdu->rem;
      sdu++;
    }
  }
  return delay/sz;
}
   
void export_buffer() {

  class_<Buffer>("Buffer", init<int, int>())
    .def("remove", &Buffer::remove)
    .def("__repr__", &Buffer::repr)
    .def("setData", &Buffer::setData)
    .def("content", &Buffer::content)
    .add_property("sz",&Buffer::getSize)
    // data is not set as property, it can be seen through repr and in setData
    // it's converted to integer, data from source is double from some distribution
    .def("config",&Buffer::config)
    .staticmethod("config")
    // variables needed for statistics processing in Python
    .def_readonly("rx", &Buffer::rx)
    .def_readonly("delay", &Buffer::del)  // del is reserved word in Python 
    ;
    
  class_<Lbuffer, bases<Buffer> >("Lbuffer", init<int,dict,int>())
    .def_readonly("drop", &Lbuffer::drop)
    .def_readonly("load", &Lbuffer::load)
    ;
}
  
