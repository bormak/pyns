#include <rand.hpp>

const int  Rnd::IA = 18807, Rnd::IR = 2836;
const long Rnd::IM = 2147483647, Rnd::IQ = 127773;
const double Rnd::AM = 1.0/IM;
const long Rnd::NDIV = 1 + (IM - 1)/32;
const double Rnd::EPS = 1.2e-7;
const double Rnd::RNMX = 1.0 - EPS;


void Rnd::seed(long _seed) {
  iy = 0;
  if (_seed < 1)
	 idum = 1;
  else
	 idum = _seed;
  for (int j = NTAB + 7; j >= 0; j--) {
	 next(); 
	 if (j < NTAB)
		iv[j] = idum;
  }
  iy = *iv;
}
	 
  


double  Rnd::get() {
  next();
  int j = iy/NDIV;
  iy = iv[j];
  iv[j] = idum;
  double temp;
  if ((temp = AM*iy) > RNMX)
	 return RNMX;
  else
	 return temp;	  
}


void Rnd::next() {
  long k = idum/IQ;
  
  // idum = IA*(idum - k*IQ) - IR*k; 
  // trick doesn't work and not needed for 64 bits
  idum = (IA*idum) % IM;
  if (idum < 0)
		idum += IM;
}


Rnd* Rand::get() {
	 Rnd* rnd = new Rnd;
	 push_back(rnd);
	 return rnd;
  }


void Rand::seed(long seed_) {
  unsigned int i = 0;
  iterator it;
  //for (iterator it = begin(); it != end(); it++)
  while (i < size()) {
	 it = begin() + i;
	 (*it)->seed(seed_*++i);
  }
}
