# router

from numpy import *
from pdb import set_trace as dbs

class Router:

    def __init__(self, nodes, reach, cost):
         
         self.neigh = dict()
         self.lcost = dict() # link costs to neighbours
         self.ncost = dict()  # node costs to dest
         self.rt = dict()    # routing tables to dest
         self.nNod = len(nodes)
                 

         for n in nodes:
             nReach =  nodes.compress(reach[n])          
             self.neigh[n] = nReach
             self.lcost[n] = cost[n][nReach]

         self.nodes = nodes


    def set(self, dest, nodes = []):
        '''
        implementation of Bellman algorithm
        '''

        dist = self.ncost.setdefault(dest, inf*ones(self.nNod))
        rt = self.rt.setdefault(dest, -ones(self.nNod,'l'))
      
        if any(isinf(dist[self.neigh[dest]])):
            dist[self.neigh[dest]] = self.lcost[dest]
            rt[self.neigh[dest]] = dest
           
        nDist = dist.copy() # new distances
        nrt = rt.copy() # new routing table

        if nodes == []:
            nodes = [n for n in self.nodes if n != dest] # exclude dest
       
        iterate = True
        while iterate: 
            for n in nodes:
                dNeigh = dist[self.neigh[n]] + self.lcost[n] # distances to neighbours
                if len(dNeigh):
                    nMin = dNeigh.argmin()  # node with minimal distance
                    nrt[n] = self.neigh[n][nMin]
                    nDist[n] = dNeigh[nMin]      
      
                ls = nDist < dist     # less distance 

                if any(ls):
                    dist[ls] = nDist[ls]
                    rt[ls] = nrt[ls]
                else:
                    iterate = False 

        self.rt[dest] = rt
        self.ncost[dest] = dist
    
