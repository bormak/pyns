#include "source.hpp"




Rnd*   Source::rnd = 0;



void Source::start(double t) {
  initState();
  notify();
  schedule(t);
}

bool Source::react(double time) {
  transit();
  notify();
  schedule(time);
  return true;
}


object Source::__repr__() {
  object msg = "srcPid %d nodePid %d srcId %d state %d \n" 
    % make_tuple(pid, node, id, st);
  return msg;
}



double Dsource::getLoad() {
  if (par.has_key("alpha"))
    return floor(_d(par["fsz"])/pow(rnd->get(), 1/_d(par["alpha"])));
  else
    return _d(par["fsz"]);
  // add truncated lognormal
}

 
  

void export_source() {


  class_<Source>("Source", init<int, int, dict>())
    .def("setRnd", &Source::setRnd)
    .staticmethod("setRnd")
    .def("__repr__",&Source::__repr__)
    .def("start",&Source::start)
    ;


  class_<Esource, bases<Source> >("Esource",init<int,int, dict>())
    ;

  class_<Msource, bases<Esource> >("Msource",init<int,int, dict>())
    ;
  
  class_<Dsource, bases<Source> >("Dsource",init<int,int, dict>()) 
    .def("wait", &Dsource::wait);
    ;
}
  
  
    
