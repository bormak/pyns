
## par['stat'] = ('pdu', 'req', 'str', 'tx', 'del', 'iter')

## par['trace'] = ('req', 'str', 'tx')

## par['src'] = ({ 'type' : 'Pareto', 'par' : [8e3, 60, 0]},
##               { 'type' : 'Exp', 'par' : [120, 1.0]})


from numpy import *

# time values in ms





class Wlan:
    sifs = 16
    tSym = 4
    ackSz = 132
    macHdr = 246
    preamble = 20
    sduHdr = 192,
    nBitSym = array([24, 36, 48, 72, 96, 144, 192, 216])

    @classmethod
    def getExtra(self):
        tDL = ceil(2*self.sifs + self.preamble)/self.tSym
        # sifs + POLL+DATA_DL + sifs
        tUL = ceil(self.sifs + 2*self.preamble)/self.tSym
        # ACK_DL+DATA_UL + sifs + ACK_UL
        return (tUL, tDL)

    @classmethod
    def getSduTime(self, frSz):
        self.sduSz = self.sduHdr + frSz
        return ceil(self.sduSz/self.nBitSym)

    @classmethod
    def getOver(self, frSz):
        '''
        calculates overhead time for each transmission mode
        in number of OFDM symbols
        input: frame size for minimal data rate
        '''
        
        hdr = [self.macHdr + self.ackSz, self.macHdr]
        tOver = []
        for n in range(2):
            tOver.append(ceil(hdr[n] + self.sduSz)/self.nBitSym)
        return tOver
        
class Wimax:
    # delault parameters
  
    fs  =  8/7
    nFFTbw = {1.25e6 : 128, 2.5e6 : 256, 5e6 : 512, 10e6 : 1024}
    nBitC = array([.5, 1, 1.5, 2, 3, 4, 4.5]) # additional modes in 16e ??
    nMode = 7
    # may be set as dynamic class with __init__('16') or ('16e')
    # cause even nBitC is different for them

    @classmethod
    def config(self, mac, bw):
        if mac == 'd':   # 802.16d
            self.tGuard = .25
            self.nDC = 192
            self.blSz = self.nDC*self.nBitC[0] # 192*.5 = 96
            self.nBlSym = self.nBitC*2 
            self.nFFT = 256
            self.extraSym = 5  # DLpreamble + FCH + DL& UL map + ttg
        else:
            self.tGuard = .125
            self.nFFT = self.nFFTbw[bw]
            self.extraSym = 4
            self.nDC = int(self.nFFT*.75)
            self.nSubCrSlot = 48
            self.nByteSlot = self.nSubCrSlot*self.nBitC/8

            if 'amc' in mac:
                self.nSubCrBin = 8  # 16
                self.nBinSlot = 6   # 
                self.nSubCh = self.nDC/self.nSubCrBin
                # number of subchannels in frequency domain
            else:
                self.nSubCh = self.nDC/self.nSubCrSlot  # 8
            
           

    
        self.tSym = self.nFFT/(self.fs*bw)*(1 + self.tGuard)  # symbol time
        
    @classmethod
    def getNumSymFr(self, tFr):
        '''
        calculates number of symbols per frame 
        input: frame time in s, bandwidth in Hz
        '''

        return int(floor(tFr/self.tSym) - self.extraSym)

    def kbSym(self, nDC = 192):    
        return self.nBitC*nDC*1e-3

  ##   @classmethod
##     def getNumBitSlot(self, nSymSlot, nDC = 192):
        
    
    
            





## def init(rand, opt):
    
##     # 802.11 effective rates in kbps (Karhima or Intersil)
##     opt['rate'] =  array([5, 6.5, 8, 10, 12, 15, 16, 17])*1e3
##     (opt['nSlot'], opt['kbSlot']) = dataRate(Wimax.tFr,Wimax.slotSz) 
##     Wimax.nRates = len(bitSym) 
