# here some usefule functions and small classes are defined
from libpyns import * 
from numpy import *
from pdb import set_trace as dbs
from types import DictType




def get(var, n):
    if isscalar(var):
        return var
    else:
        return var[n]


    
        
 
 


    


# move to router             
def bellman(dest, nodes, N, link, lcost):
    # make a router method
  
    # N = len(link) # number of nodes

    dist = inf*ones(N) # shortest distance,
    dist[link[dest]] = lcost[dest] # immediate neigbours
    nDist = dist.copy() # new distances
    
    rt = -ones(N) # routing table
    rt[link[dest]] = dest   
    nrt = rt.copy() # new routing table
    # nodes = [n for n in src if n != dest]
                                             
    iterate = True
    while iterate: 
        for n in nodes:
            dNeigh = dist[link[n]] + lcost[n] # distances to neighbours
            if len(dNeigh):
                nMin = dNeigh.argmin()  # node with minimal distance
                nrt[n] = link[n][nMin]
                nDist[n] = dNeigh[nMin]      
      
        ls = nDist < dist     # less distance 

        if any(ls):
            dist[ls] = nDist[ls]
            rt[ls] = nrt[ls]
        else:
            iterate = False 

    return rt, dist
        

## def convert(in):
##     from Numeric import array
##     return array(x.tolist())

## def nplot(x,y,*args):
##     from matplotlib import plot
##     plot(convert(x),convert(y),args)
        

def show(m):  # put as a separate script in py
    '''
    shows a matrix m in tktable
    '''
    import Tkinter, tktable
    # exec('global ar' + i)
    global ar2

    yS,xS = m.shape
    # add title row and column
    m = concatenate((arange(xS).reshape(1,xS),m),0)
    yS += 1
    m = concatenate((arange(yS).reshape(yS, 1) - 1, m),1)
    xS += 1

   
    c,r = [axis.ravel() for axis in indices(m.shape)] # columns and rows
    
    root = Tkinter.Tk()
    # exec('ar' + i + ' = ArrayVar(root)')
    ar2 = tktable.ArrayVar(root)
    
    for n in range(m.size):
        # exec('ar' + i + ".set(str(c[n]) + ',' + str(r[n]), m[c[n],r[n]])")
        index = '%i,%i' % (c[n],r[n])
        if m[c[n],r[n]] != -1:
            ar2.set(index, m[c[n],r[n]])
    ar2.set('0,0','')
   ##  exec('t = Table(root,variable = ar' + i + \
##          ",cols = m.shape[1],rows = m.shape[0],colstretch = 'all',rowstretch = 'all')")
    scx, scy = (0,0)
    
    if m.shape[0] > 50:
        scy = Tkinter.Scrollbar(root, orient = Tkinter.VERTICAL)
        scy.pack(side = Tkinter.RIGHT, fill = Tkinter.Y)

    if m.shape[1] > 50:
        scx = Tkinter.Scrollbar(root, orient = Tkinter.HORIZONTAL)
        scx.pack(side = Tkinter.BOTTOM, fill = Tkinter.X)
    

    t = tktable.Table(root,
              variable = ar2,
              cols = xS,
              rows = yS,
              selectmode = 'extended',
              colstretch = 'all',
              rowstretch = 'all',
              titlerows=1,
              titlecols=1,
              width = 30,
              colwidth = 6)
              # xscrollcommand = scx.set
          
    
    t.pack(expand = 1,fill = 'both')
    t.tag_configure('title', anchor='w', bg='red', relief='sunken')
    if scx:
        t.configure(xscrollcommand = scx.set)
        scx.config(command = t.xview)
    if scy:
        t.configure(yscrollcommand = scy.set)
        scy.config(command = t.yview)
    
    if '__len__' in dir(var):
        return var[n]
    else:
        return var

############################################ Result ###########################################################################
    
class Result(cResult):

    def init(self, shape, keys = 0):  
        # create arrays for number of runs
        for name,sz in self.names.items():
            if sz > 1:
                size = shape + [sz]
                # extend shape with the result size
            else:
                size = shape
            if not isscalar(keys):
                self.val[name] = {}
                for key in keys:
                    self.val[name][key] = zeros(size)
            else:
                self.val[name] = zeros(size)


    def __repr__(self):
        out = str()
        for key,value in self.val.items():
            out += '\n %s \t' % key
            if isinstance(value, DictType):
                for par,val in value.items():
                    out += '\n %s \t %s' % (par,array2string(val.squeeze(),precision = 2))
                out += '\n'
                    
            else:
                out += '%s' % array2string(value.squeeze(),precision = 2)
        out += '\n'
        return out
    
        
    def write(self,argv):
        if self.out != None:
            if self.out != 'std':
                import os, cPickle
                fn = os.path.join(self.dir,self.out + '.dat')
                self.val['argv'] = argv
                cPickle.dump(self.val, open(fn,'w'))
            else:
                print self.__repr__()

    def average(self, res, n, par = []):    

        for key,value in self.val.items():
            if isinstance(value, DictType):
                result = value[par]
            else:
                result = value

            if self.names[key] > 1:        
                for i in range(self.names[key]):
                    result[n,i] = mean(res.val[key][:,i])
        
        



def loadResult(subdir, file):
    import os, cPickle
    file = os.path.join('/home/bma/pr/res',subdir,file + '.dat')
    r = cPickle.load(open(file))
    return r
    

def initTrace(self, nSample):       
    for name in self.names:
        self.val[name] = zeros(nSample,'d')
    self.sN = 1 # sample number
    self.nTr = len(self.names)

Trace.init = initTrace



class GF:
    def __init__(self,p):
        self.p = p # prime

    def add(self,x,y):
       return (x+y) % self.p   

    def mul(self,x,y):
        return x*y % self.p

    def eval(self, poly, x):
         y = poly[0]
         for i in range(1, len(poly)):
             if i == 1:
                 val = x
             else:
                 val = self.mul(val,x)
             y = self.add(y, self.mul(poly[i],val))
         return y
        
   
                   



    



def fcomplex(x,y = 0):
    return complex(x,y)
# needed because numpy vectorize needs func_code and
# can't use builtin functions

vcomplex = vectorize(fcomplex)
# needed due to ignorance how to create compex array from
# real and imag parts in one sentence in numpy


# redundant due to comlex_, nevertheless useful as an example of
# vectorizing

## def replace(arr, cond, val):  
##     return put(arr, compress(cond.flat, arange(len(arr.flat))), val)
# not needed because of putmask
