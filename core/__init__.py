# init for pysim module
# import sys, os
# print sys.path.append(os.getcwd() + '/lib')

from numpy import *
from sim import *
from router import *
from wlnet import *
from netpar import *
from pdb import set_trace as dbs



## default values


# command line parameters
# cannot start with empty line
clp = '''Sim.nRun,1,number of runs,-n
Sim.seed, 0, starting seed,-s
Sim.type,"cppy",simulation type,--stype
Sim.log,"none", log level,--log 
Thread.tStop, 0, simulation end time, -t
Thread.tSample, 0, trace sampling time, --tsample
Result.out, std, output stream,-o
WLnet.nNod, 0, number of nodes, --nn
WLnet.ple, 4, path loss exponent, --ple
WLnet.pMax, .8, maximum power in Bell, --mp
Msource.nSt, 5, number of states, --nst
Thread.hist,False, histogram, --hist
'''

Result.dir = '/home/bma/pr'
# extends with program module's results directory


# generic parameters






        

