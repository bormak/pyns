from sim import Thread
from simlib import *
from numpy import *
from pdb import set_trace as dbs



class WLnet(cWLnet, Thread):

    # default parameters
    kT = 1.38e-23*300 # Boltzmann constant for T = 300 K
    sysNoise = 1.2 # system noise in Bells
    corrDist = 45 # lognormal correlation distance in m
    sigma = 8     # lognormal std in dB
    # antenna lobes for beam switching
    lobe = array([[-pi, -pi/2, 0, pi/2, pi],[0, pi/4, pi/2, 3*pi/4, pi]])
    beam = 1   # number of beams
    jLoc = 0   # jammer location
    aG = 0     # antenna gain


    @classmethod
    def config(self, arg):

        Thread.config(arg)
        # creates rnd, calls cThread::config

        if len(arg):   # first call
            self.aNoise = self.kT*self.bw*10**self.sysNoise
            self.noise = log10(self.aNoise)
            self.rnd = random # numpy random module for locations generation
            self.rand.append(self.rnd)  # append rnd to generators list rand

            if self.beam > 1:
                self.beam = self.lobe.size/2 - 1
                self.bsw = True
            else:
                self.bsw = False
            
               

            if self.topo == 'grid':
                self.gridPar = r_[self.gridPar]*self.getMaxDist(0)
        else:
            if not dict(self.__dict__).has_key('pid'):
                cWLnet.config(dict(self.__dict__)) # doesn't call cThread::config !


        
        
              
   

   
   
    
    def __init__(self):
        cWLnet.__init__(self)

        if self.topo == 'line':
            self.size = self.nNod
            self.genDist()  # generate distances
        else:
            self.size = (self.nNod, self.nNod)
            self.genLoc()  # generate locations
            self.getDist() # get distances
            # different for cellular and mesh

        self.setLinkGain()

        self.size = (self.nNod)  # to save typing,
        # when it could be only nNode ?
        # for cellular it is (nBs,nNod
         
        if self.bsw:
             self.getSector()
             # lob borders for 1 and 2-lob sectors        
             self.sec = self.getSector(dir)      

        
        if all(self.jLoc != 0):
             self.setJam()          
        else:
            self.pNa = ones(self.size)*self.aNoise
            self.pN =log10(self.pNa)
            # even it might be faster to call getNoise with 3 cases for scalar, vector and matrix noise
            # it is simpler to have a matrix for all 3 cases
         
    
        self.rxSNR = zeros(self.nNod) # received snr    
         
        self.mode = zeros(self.size,'l') # rate mode
        self.pT = -Inf*ones(self.size)
        self.pTa = zeros(self.size)
        self.pTr = dict()  # what are they
        self.ra = True  # what's this ?
       

    def genLoc(self):
        if self.topo == 'grid':
            self.makeGrid(self.gridShape)
        # other topologies

    def getDist(self):
        diff = loc - loc.transpose()
        self.dist = abs(diff)  # distances
        self.dir = angle(diff)
        
        
        

    def setLinkGain(self):
        # later include lognorm option
        self.lG =  -self.ple*log10(self.dist)
        self.lGa = 10**self.lG
        
        

    def getPow(self):
         # power for directional transmission and reception
         pT = self.pN + self.reqSNR[0] - self.lG - 2*self.aG
         pT = maximum(pT, pT.transpose()) # bidirectional links
         # dbs()
         pT[pT > self.pMax] =  -inf 
         return pT

    
    def getSector(self):
        sec = zeros((self.size))
        if self.nLob == 2:
            dir = abs(self.dir)
        for k in range(1, self.beam):
            sec[(dir >= self.lobe[self.nLob - 1][k]) & (dir < self.lobe[self.nLob - 1][k + 1])] = k
        self.sec = sec
        self.aGa = 10**self.aG
        
    def setJam(self):
        
        pNa = squeeze(ones((self.nNod, self.beam)))*self.noise
        # absolute noise power

        for n in range(self.jLoc.size): # number of jammers
           
            jD = abs(self.jLoc[n] - self.loc)
            jPa = 10**(log10(self.jPtx) - self.ple*log10(jD)) # absolute jammer power
            if self.beam > 1:
                jDir = angle(self.jLoc[n] - self.loc)
                # arriving sector for jammer
                jSec = self.getSector(jDir)
                jPaB = reshape(repeat(jPa,self.beam),(self.nNod,self.beam))
                # received jamming power per beam
               
                for k in range(self.nNod):
                    jPaB[k,jSec[k]] = jPa[k]*10**self.aG  # amplified by anteena gain
                pNa += jPaB
            else:
                pNa += jPa

        if self.beam > 1:
            for n in range(self.nNod):
                self.pNa[n,:] = pNa[n,self.sec[n,:]]
        else:
            self.pNa = reshape(repeat(pNa,self.nNod),self.size)



    def setMode(self, lnk):
        '''
        sets transmission modes for link
        link is either scalar if all vectors 1dim or tuple if 2dim
        '''
      
        
        pTall = self.reqSNR - 2*self.aG + self.pN[lnk] - self.lG[lnk]
        self.mode[lnk] = pTall.searchsorted(self.pMax) - 1 # highest posssible mode
        self.pT[lnk] = 10**pTall[self.mode[lnk]]
            




            

    

           
           
    def setRxPow(self, edge, snr):
       # use edge dictionary
       self.pTa = 10**self.pT
       for link in edge.keys():   # transmitting link
           if self.beam == 1: 
               self.pRa[link] = self.pTa[link]*self.lGa[link[0],:]
               self.pR[link] = log10(self.pRa[link][link[1]])  # pR(i,j)
           else:
               self.pRa[link] = dict()
               for rLink in edge.keys():  # received link                
                   self.pRa[link][rLink] = self.pTa[link]*self.lGa[link[0],rLink[1]]
                   if self.sec[link] == self.sec[link[0],rLink[1]]:
                       self.pRa[link][rLink] *= self.aGa  # transmitting sector
                   if self.sec[rLink[1],rLink[0]] == self.sec[rLink[1],link[0]]:
                           self.pRa[link][rLink] *= self.aGa # receiving sector
               self.pR[link] = log10(self.pRa[link][link])




    def genLognMap(self, nP):

        '''
        function for generation of lognormal map, adapted from rune Matlab
        parameter - number of points,
        '''

        from scipy import fftpack as f
        s = nP/2
        v = mat(f.fftshift(linspace(-s, s, nP+1)[0:nP]))
        r = abs(v + v.transpose())
        acf = exp(-r/self.corrDist)
        rho = exp(-1/self.corrDist)
        sigma = sqrt(self.sigma**2/(1 - rho**2)) # correction
        af = f.fft2(acf)
        norm = random.normal(scale = self.sigma, size = acf.shape)
        z = f.ifft2(f.fft2(norm)*sqrt(complex_(af.real)))
        return z.real       

     
    def genDist(self):
        maxDist = self.getMaxDist()
        self.dist = self.rnd.uniform(1,maxDist,self.nNod)
       
            
    def getMaxDist(self):
        # link budget margin for slow and fast fading in bells
        return 10**((self.pMax - self.noise - self.reqSNR[0] - self.margin)/self.ple)
    
                                                     
        
   
                       


    def makeGrid(self):
        # move to wlnet
        d0 = round(self.gridPar[0]) # bug in Numpy for some values, like .5 for ple 3
        delta = self.gridPar[1]
        shape = self.gridShape
        grid = mgrid[0.1 : d0*(shape[0]+1): d0, 0.1 : d0*(shape[1]+1): d0]
        # can't start from 0 bug ?
        grid += random.rand(2, shape[0], shape[1])*delta
        grid = vcomplex(shape[0],shape[1])
        loc =  grid.reshape((1, prod(shape)))
        self.loc = reconvert(opt["nByteSlot"],nByteSlot);shape(repeat(loc,self.nNod),self.size) # locations





##     def getMode(self, snr, tr, rv, interf = -1):
##         # interf supposed to be used for waterfilling but it didn't work
##         if interf == -1:
##             noise = self.pN[tr,rv]
##         else:
##             noise = log10(interf + self.pNa[tr,rv])
        
##         pTall = snr - 2*self.aG + noise - self.lG[tr, rv]
##         hMode = pTall.searchsorted(self.pMax) - 1 # highest posssible mode
##         if int == -1:
##             self.pT[tr,rv] = pTall[hMode]
##             return hMode
##         else:
##             return (hMode, 10**pTall[hMode])




 ##    def setRate(self,edge,link):
        
        

##                    self.pRa[link] = dict()
##                    for rLink in edge.keys():
##                        pRa = 10**self.pTr[link]*self.lGa[link[0],rLink[1]]
##                        if log10(pRa[0] + self.pNa[rLink]) + snr + self.lG[rLink] < self.pMax:
##                            self.pRa[link][rLink] = pRa
##                else:


class WLnode(cWLnode):


    def __init__(self,id):

        if  dict(self.__dict__).has_key('pid') == False:
            cWLnode(id)
        # if not created by a child
        # nSrc, srcType  should be determined from service mix distribution, so far static parameters
        # wr

    def connSrc(self, sc, dest = 0, tStart = 0):
        '''
        connect sources for specific service
        argument - integer service class index in service specification list

        '''
        if type(sc) == str:
            sc = self.service.keys().index(sc) # convert to order if service name

        srv = self.service[sc]
        self.nSrc = srv['nSrc']
        srcType = Thread.src[srv['srcType']]
        self.setup()  # cpp method, creates data array

        from simlib import Lbuffer
        

        for n in range(self.nSrc):
            if srv['buf']['type'] == 'Lbuffer':
                self.buf.append(Lbuffer(self.id, srv['buf'], dest))
            else:
                self.buf.append(Buffer(self.id, dest))
            
            exec 'self.src.append(%s(%d,%d, %s))' % (srv['srcClass'],self.pid,n,srcType)
            if tStart == 0:
                self.src[n].start(tStart)
           


    # here py is used to distinguish between python and C++ children
    # that allows pySetup be virtual in Python and setup virtual in C++
    # otherwise concrete parent for setup call must be given like CWlnode.setup
    # which would preclude a call of setup from cWLnode children

  
