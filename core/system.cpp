#include "system.hpp"
#include<iostream>
#include <string>
#include <cstdarg>


using namespace boost::python;

Scheduler *Event::sched, *Thread::sched;
int Process::nProc = 0;
std::vector<Process*> Process::addr(nProcMax);
object   Thread::res(0);
double Thread::tStop = 0, Thread::tStep = 0, Thread::tSample = 0;
// list Thread::rand(0);




void export_WLnet();
void export_source();
void export_buffer();

Rnd* Process::getRnd(object randObj) {
  Rand& rand  = extract<Rand&>(randObj[0]);
  // 0 is location of C++ random generators deque 
  return rand.get();
}


void Process::signal(std::string type, ...) {
  va_list ap;
  va_start(ap, &type);
  if (!handle(type, ap)) {
    std::cerr << "signal " << type << " not recognised \n";
    exit(0);
  }   // make with exception
  va_end(ap);
}
  






Scheduler::Scheduler() : std::priority_queue<class Event>() {
  Event::bind(this);
  Thread::bind(this);
}

Event Scheduler::next() {
  Event ev = top();
  pop();
  return ev;
}


void Scheduler::clear() {
  while (!empty())
    pop();
}

void Thread::start(std::string simType) {

  int evGen = -1; // Python
  if (simType == "cpp")
    evGen = 0; // itself
  
  if (tStop) {
    new Event(tStop, "stop", evGen); // 0 - address of Thread
    if (tStep)
      new Event(tStep, "step", evGen);
    if (tSample)
      new Event(tSample, "sample", 0); 
    // sampling always done in C++ 
  }
}
    
    

void Thread::config(dict opt) {
  tStop = _d(opt["tStop"]);
  tStep = _d(opt["tStep"]);
  tSample = _d(opt["tSample"]);
}
  


bool Thread::run() { 
  while (!sched->empty()) {
    Event ev = sched->next();
     
    if (!react(ev)) {
      std::string msg = extract<std::string>(ev.getMsg());
      std::cerr << "Event type " << msg << " not recognised \n";
      return false;
    }
  }

  return true;
}

bool Thread::react(Event& ev) {
  double t = ev.getTime();
  str type = ev.getMsg();
  

  if (type == "stop")
    stop(t);
    
     // why stop() doesn't work here
  else if (type == "step")
    step(t);
   
  else if (type == "sample") {
    sample(t);
    trace._f(sample)(res._a(val)); 
    new Event(t + tSample, "sample", 0);
  } 
  
  else
    return false;
  return true;
}
    
void Trace::sample(dict res) {
  object key = val.keys();
  for (int n = 0; n < nTr; n++)
    val[key[n]][sN] = res[key[n]][0];
  sN++;
}
    
 


bool Event::react() {
    if (Process* proc = Process::getAddr(pid))
        // precludes reacting to dead processes
       // when process is deleted it puts -1 in its addr[pid]
      return proc->react(*this);
    else
      return true;
}


BOOST_PYTHON_MODULE(libpyns) { 
  numeric::array::set_module_and_type("numpy","ndarray");

  
  
 
  export_buffer();

  class_<Process>("Process")
    .def_readonly("pid",&Process::pid)
    ;

  export_source(); // can only be called after class_<Process>

  class_<Event>("Event", init<double, str, int>())
    .def("python",&Event::python)
    .def("react",&Event::react)
    .add_property("time", &Event::getTime)
    .add_property("msg", &Event::getMsg)
    .add_property("pid", &Event::getPid) // for debugging
    ;

  class_<Scheduler>("Scheduler")
    .def("empty", &Scheduler::empty)
    .def("clear", &Scheduler::clear)
    .def("next", &Scheduler::next)
    ;

  
  class_<Thread>("cThread")
    .def("start",&Thread::start)
    .def("run",&Thread::run)
    .def("react",&Thread::react)
    .def("stop",&Thread::stop)
    .def("finish",&Thread::finish)
//     .def("addResVal",&Thread::addResVal)
//     .def("setResVal",&Thread::setResVal)

   //  .add_static_property("res",&Thread::setRes,&Thread::getRes)
//     // doesn't work of some reason
//     .add_static_property("trace",&Thread::setTrace,&Thread::getTrace)
    .def("config",&Thread::config)
    .staticmethod("config")
    .def("setResult",&Thread::setResult)
    .staticmethod("setResult")
    .def_readwrite("trace",&Thread::trace)
    .def_readwrite("t", &Thread::t)
    ;
 
  
  class_<Rand>("Rand")
    .def("seed", &Rand::seed)
    ;
 
  
  class_<Result>("cResult")
    .def("set",&Result::set, Result_set_member_overload())
    .def("add",&Result::add, Result_add_member_overload())
  //   .def("set", &Result::set)
//     .def("add", &Result::add)
    .def_readwrite("val",&Result::val)
    .def_readwrite("rN",&Result::rN)
    ;

  class_<Trace>("Trace")
    .def("sample", &Trace::sample)
    .def_readwrite("val",&Trace::val)
    .def_readwrite("nTr", &Trace::nTr)
    .def_readwrite("sN", &Trace::sN)
    ;
 
  export_WLnet();
}
