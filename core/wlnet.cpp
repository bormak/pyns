#include "wlnet.hpp"

int WLnet::beam = 1;
int WLnet::nNod = 0;


void WLnet::transmit(double time, object grant) {
  // how it would work for multicell ??
  
  for (int n = 0; n < nNod; n++) {
    WLnode& aNode = extract<WLnode&>(node[n]);
    aNode.transmit(time, grant[n]);  // grant for n-th node
  }
}


void WLnet::load(double time) {
  
  for (int n = 0; n < nNod; n++) {
    WLnode& aNode = extract<WLnode&>(node[n]);
    aNode.load(time);  
  }
}



// function rxCell for cellular and rxOFDMA provided later


// later generalise rxMesh to generic without beam switching and jamming
// they can be added in Wmesh
void WLnet::rxMesh(object tr, object rv) {
  // intended receiver of tr[n] is rv[n]
  // receive from tr to rv, count SNR for all rv
  
  tuple nn, kk; // links n -> n, k -> k
  int nRv; // n-th receiver
  
  int nS = len(tr); // number of stations transmitters = receivers
  double* interf = new double[nS];  // total interference
   
  for (int n = 0; n < nS; n++) {  // iterate over receivers
    nRv = i(rv[n]); // n-th receiver
    nn = _e(_i(tr[n]),nRv);
    if ( (tr._f(__eq__)(nRv))._f(any)() ) 
      rxSNR[nRv] = Conflict;  // if nRv transmits it can't receive (only simplex)
    // -10 used to increment conf statistics
    else {
       
      interf[n] = getNoise(nn); // initially noise + jamming
      // how to make generic for noise scalar, vector, matrix
      for (int k = 0; k < nS; k++)
	if (n != k) {
	  kk = e(i(tr[k]),i(rv[k]));
	  // later make virtual to avoid beam in generic rxMesh
	  if (beam == 1)
	    interf[n] += d(pRa[kk][nRv]);
	  else
	    interf[n] += d(pRa[kk][nn]);

	}
        
      rxSNR[nRv] = d(pR[nn]) - log10(interf[n]);
    }
  }

  delete interf;
  // it seems x[i] i automatically converts to int ?
}
  
double WLnet::getNoise(tuple lnk) {
 double noise = _i(pNa._a(ndims)) == 1 ? 
   _d(pNa[_i(lnk[1])]) : _d(pNa[lnk]);
 return noise;
}



void WLnet::config(dict opt) {
  beam = _i(opt["beam"]);
  nNod = _i(opt["nNod"]);
  // add later other static parameters
}


bool WLnode::handle(std::string type, va_list ap) {
  if (!type.compare("src")) {
    int srcId = va_arg(ap, int);
    buf[srcId]._f(setData)(va_arg(ap, double));
    return true;
  }
  return false;
}
  

void export_WLnet() {
  class_<WLnet, bases<Thread> >("cWLnet")
    .def("config", &WLnet::config)
    .staticmethod("config")
    .def("transmit", &WLnet::transmit)
    .def("load", &WLnet::load)
    .def("rxMesh",&WLnet::rxMesh)
    .def_readwrite("pT",&WLnet::pT)
    .def_readwrite("pTa",&WLnet::pTa)
    .def_readwrite("lG",&WLnet::lG)
    .def_readwrite("lGa",&WLnet::lGa)
    .def_readwrite("pN",&WLnet::pN)
    .def_readwrite("pNa",&WLnet::pNa)
    .def_readwrite("rxSNR",&WLnet::rxSNR)
    .def_readwrite("pR",&WLnet::pR)
    .def_readwrite("pRa",&WLnet::pRa)
    .def_readwrite("node", &WLnet::node)
    // .def_readwrite("beam",&WLnet::beam)
    ;

  class_<WLnode, bases<Process> >("cWLnode", init<int>())
    .def("setup", &WLnode::setup)
    .def("load", &WLnode::load)
    .def("transmit", &WLnode::transmit)
    .def_readwrite("src", &WLnode::src)
    .def_readwrite("buf", &WLnode::buf)
    .def_readwrite("nSrc", &WLnode::nSrc)
    .def_readwrite("id", &WLnode::id)
    ;
}
