# simulation class
from simlib import *


boolean = ['False','True']
boolAct = ['store_true','store_false']
# own = ['Sim','Result']
    

class Sim:

    def __init__(self, module, arg):
        ''' takes module
         
        '''
        if type(arg) == str:  # called from gdb
            arg = arg.split(' ')
        
        self.mod = module
    
        exec 'from %s import clp, default, threadClass' % module
        self.threadClass = threadClass
       
        exec 'from %s import %s' % (module, self.threadClass)
        self.config(clp, arg) # assigns command line parameter values
        default()  # executes module's default, assigns default values
        # run after sim.congig to allow overiding of pyns.clp default values (tStep)      
        exec '%s.config(arg)' % self.threadClass
        
        
        
      
   
            
            
        
           
        

        
        


    ## def multirun(self, thread, par):

##         lPar = len(par)

##         if lPar == 0:
##             res = run(thread)
##             for key,value in res.val.items():
##                 print "%s \t %.2f \n" % key,value
##         else:
##             p = zeros(lPar)
##             sh = []   # results shape
             
##             for k in range(lPar):
##                 exec('p[k] = (' + thread + '.' + par[0] + ',)') # make tuple
##                 sh.append(len(par[k])

##             sh = tuple(sh)            
##             res = Result()
##             res.init(sh)
            
##         # ffs.


    def run(self, index):
        ''' run a number of runs with different seeds
        '''
        from sys import stderr
        if 's' in self.log:
            stderr.write('%s %s \n' % (Result.out, index))


        exec 'from %s import %s' % (self.mod, self.threadClass)
        exec '%s.config([])' % (self.threadClass)
        # config without argv can change parameters between calls to run
           
        
 	for n in range(self.nRun):
            for rnd in Thread.rand:
                 rnd.seed(prime[self.seed + n])
         
            exec 'self.thread = %s()' % (self.threadClass)
            self.thread.run(n)
            # if seed is taken for other run than
                    
        self.thread.finish() # something to finish all runs
        
        return self.thread.res

        #return self.t     
         
                
   

    def config(self, clp, argv):
        import core, csv, optparse
        clp = core.clp + clp  # command line parameters
        parl = csv.reader(csv.StringIO(clp)) # parameters list
        parser = optparse.OptionParser()      


        for args in parl:
            val = args[1] # value
            if val in boolean:
                act = boolAct[boolean.index(val)]  # for switch without argument
            else:
                 act = 'store'
            switch = args[3].strip()
            parser.add_option(switch,dest=args[0],action = act,help=args[2],default=args[1])
            
        
        # read command line options
        (opts,wrong) = parser.parse_args(argv) # returns tuple
        self.optsd = opts.__dict__ # options dictionary

        coreDir = dir(core)
      
        for var, val in self.optsd.items():
            
            if type(val) == str:
                val = val.strip()
                val = val.replace(' ',',')  # needed for tuples
                if not val.isdigit() and val not in boolean and val.isalnum():
                    # requires e+ notation          
                    val = repr(val)
                else:
                    val = str(val)


            _class = var.split('.')[0]
           
            if _class in coreDir:
                exec 'from core import %s' % _class
            else:
                exec  'from %s import %s' % (self.mod, _class)

            if type(val) == str and ':' in val:
                    exec '%s = r_[%s]' % (var,val) # make array
            elif type(val) == str and ',' in val:
                    exec "%s = val.split(',')" % var
            else:
                try:
                    exec '%s = %s' % (var,val)
                except  NameError:               
                    exec '%s = %s' % (var, repr(val))
        import os
        Result.dir = os.path.join(Result.dir,self.mod,'res')
        
        
            
        
prime = (13, 47, 73, 147, 113, 131,    137,    139,    149,    151,    157,
         163,    167,    173, 179,    181,    191,    193,    197,    199,    211,    223,
         227,    229, 233,    239,    241,    251,    257,    263,    269,    271,    277,
         281)


# find a prime generation program
   
########################################################

class Thread(cThread):

    @classmethod
    def config(self, arg):
        if len(arg):   ## first config
            self.rand = [Rand(),] # c++ Rand as first member
            self.sched = Scheduler()
            # from libpyns import cSource
            Source.setRnd(self.rand) # extracts rand[0]
            if self.src.has_key('vbr'):
                self.configVBRsource(self.src['vbr'])

            if 'db' in Sim.log:  # debugging
                print "%s imported" % self.mod # for gdb
                dt = self.tStop/10
                self.times = arange(.0,self.tStop + dt, dt)

            cThread.config(dict(self.__dict__))  # static function

                
            
        else:  # reconfig
          self.res = Result()
          self.res.init([Sim.nRun])
          # list needed for possible dimension extension
          cThread.setResult(self.res)
          if self.hist:
              self.makeHist()  # histograms made every sim.run
         
              
          
          


    def __init__(self):
        if not dict(self.__dict__).has_key('pid'):
            cThread.__init__(self)
        
       
        
        

    def run(self, runN):
       
        self.res.rN = runN  # run number
        
        if self.proceed:

            # returns false if Monte-Carlo or
            # something precluded simulation to run like no connectivity in mesh

            # dbs()
            self.start(Sim.type)
                    
            if self.tSample:  # tracing                
                self.trace = Trace()
                self.trace.init(self.tStop/self.tSample + 1)
                # trace object created in C++ constructor
                      
                    
            if Sim.type == 'cpp':
                if not cThread.run(): # simulation loop in C++
                    exit()                
            else:   # simulation loop in Python
                       
                if 'db' in Sim.log:  # debug
                    now = 0
                while not self.sched.empty():
                    ev = self.sched.next()
                    # show simulation time
                    if 'db'in Sim.log and ev.time > self.times[now]:
                        sys.stderr.write('%.1f ' % self.times[now])
                        now += 1

                    if ev.python():
                        # dbs()
                        exec 'self.%s(%f)' % (ev.msg,ev.time)                 
                    else:
                        # dbs()
                        if not ev.react():
                            # executed in C++, return False if ev.msg not fired
                            print "event type %s is not known \n" % ev.msg
                            exit()
                               
                                

    def step(self,t):
        self.update(t)
        Event(t + self.tStep,'step',-1)  # -1 means python

        
    

    def end(self):
        if self.tSample:
            import cPickle
            file = os.path.join(Result.dir,'.tr')
            cPickle.dump(self.trace.val, open(file,'w'))
        return self.res  # check if res updated

    @classmethod
    def makeHist(self):
        
        from wlnet import WLnode
        # put service to Thread

        for srv in WLnode.service:
            buf = srv['buf']
            if buf['type'] == 'Lbuffer':
                tBin = buf['tBin']
                if tBin == 0:
                    buf['tBin'] = tBin = self.tStep
                nBin  = int(ceil(buf['maxDelay']/tBin))
                buf['hist'] = zeros(nBin)

    def countHist(self):
        from wlnet import WLnode

        stat = []

        for srv in WLnode.service:
            buf = srv['buf']
            if buf['type'] == 'Lbuffer':
                prob = buf['hist']/buf['hist'].sum()
                val = linspace(buf['tBin'],buf['maxDelay'], prob.size)
                # mean and std
                mean = (prob*val).sum()
                stat.append((mean, sqrt((prob*val**2).sum() - mean**2)))

        return stat
                
        
        
                        
    
           
    @classmethod
    def configVBRsource(self,p):
        
        
         nSt = p['nSt']
         st = arange(nSt) # states
         dim = (nSt,nSt) # dimension

         if not p.has_key('rate'):  # relative state rate
             p['rate'] = double(st + 1) # rate proportional to number of states

         if not p.has_key('trMat'):  # transition matrix not given
             trMat = ones(dim,'d') # transition matrix 
             if p['dist'] == 'e':  # equal transition probability to any state          
                 trMat[st,st] = 0;  # put 0 to diagonal
             else:  # symmetrical for neighbourning states
                 invst = arange(nSt-1,0,-1) # what invst means inverse state ?
                 st1 = st + 1
                 for i in st:
                     trMat[i] = r_[st1[nSt-i-1 : -1],0,invst[0 : nSt-i-1]]

         # cumulative transition rate
         cumRate = sum(trMat,1);  
         # matrix of transition probabilities
         trPr = trMat/cumRate.repeat(nSt).reshape(dim)

         p['trMat'] = cumsum(trPr,1);

         # mean time in state
         tSt = 1./cumRate  # time in state 
         stPr = tSt/sum(tSt) # state probability
         p['meanRate'] = sum(p['rate']*stPr)
         p['tSt'] = tSt  # needed in cpp

        

    @classmethod
    def configVBRload(self, trRate):
        p = self.src['vbr']
        p['load'] = p['rate']*trRate/p['meanRate']
        
     # called by derived classes if trRate is changed between runs

        # self.rand = [Rand(),]
##         # list of random generators, used for seeding
##         # first member is the C++ deque of rnd generators
##         # it isn't included in Thread to allow use of numpy random generators
##         # also it's not a dictionary object for easy call in C++ without Rand extraction
##         self.sched = Scheduler()
#        init(self.rand, arg) # inits module's classes

    
          
    
    

        
        

        

    
            
            
                
            
        
        
