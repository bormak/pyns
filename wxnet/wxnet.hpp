#ifndef WXNET_HPP
#define WXNET_HPP


#include <wlnet.hpp>


class WXBS : public Process {
public :
  void greedy(object sorted, object nBlocksSymbol);
  bool handle(std::string type, va_list ap);
  static void config(dict opt, bool init);
  void reserve(int nod, int lnk, int k, int mode = -1);
  void assign(int link);
  bool assignChan(int lnk, int nod, int ch);
  void ulPow();
  void dlPow(object gain, object pTreq);
  object req;  // node requests
  object mdel; // mean delay
  object grant;
  int nBuf;
  object gain;
  object srtGain;
  object free;
  object pT;
  object grSlot;
  object active;
  object prior;
  object pTreq;
  object alloc;
  object rate;
  
protected :
  int* wfill(object gain, object pTreq, int nCh, double pMax); 
  int* greedyPow(object gain, object pTreq, int nCh, double pMax);
  static int nSymFr;  // number of free symbols per frame
  static int nBlPdu;  // number of burst blocks per PDU, 1 for fragmentation
  // 1 block  = 12 bytes, 96 bits corresponds to 192  subcarriers for BPSK .5 bits per carrier
  static int* nByteSlot;
  static object pR; // required received power per bin
  static int nNod;
  static int nSubCh;
  static int nMode;
  static double noise;
  static double* pMax;
  static double* dEb;
  static bool wf;  // water filling
  static bool pr; // proportional share
  static bool iu; // iteration over users
  static bool fp; // fair proportional from Nguen
  static bool mg; // maximal gain assignment
  static bool ei; // energy increment
  static bool gp; // greedy power allocation
};

class WXnode : public WLnode {
public:
  WXnode(int nodeId, int bsPid) : WLnode(nodeId), bs(bsPid) {;}
  // void setup();
  void load(double time);
  void transmit(double time, object grant);
  // void handle(std::string type, va_list var);
  // variables accessed from Python
  // object buf;  // buffers
  // int nSrc;  // number of sources
protected:
  int bs;
};

#endif



    
