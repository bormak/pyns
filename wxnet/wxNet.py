# name wxNet is to distinguish from directory wxnet
# this allows to simulate wxsim in this directory which is needed for gdb,
# gdbs script resides in the directory, with the same file's name as directory
# import wxnet would happen from the file and not from the module

import sys
print sys.path

from itpy import Fchan
from core import *
from libwxnet import *
from wxbs import WXBS


class WXnet(WLnet):

    @classmethod
    def config(self, arg):

        init = False

        if len(arg):
            if '--nn' not in arg:
                WLnet.nNod = 12
            Thread.tStep = self.tFr[self.mac[0]]
            init = True

        
        WLnet.config(arg)  # calls Thread 

        if init:   # first call 
            # here parameters different between ofdma and tdma modes are given

            Wimax.config(self.mac, self.bw)

            if 'e' in self.mac:
                WXBS.ofdma = self.ofdma = True
                Fchan.tSym = Wimax.tSym
                Fchan.config(self.rand) # appends itpp random generator
                if 'amc' in self.mac:
                    WXBS.pR = 10**self.reqSNR*self.aNoise/Wimax.nSubCh 
                    # required received power per subchannel for each mode
                    WXBS.amc = self.amc = True
                    self.pMax = 10**self.pMax # convert to absolute
                else:
                    WXBS.amc = self.amc = False
                    
            else:
                WXBS.ofdma = self.ofdma = False
                # self.blSz = wimax.blSz/8  # block size in bytes
                self.nByteSym = wimax.nBlSym*8           
                
            
            Buffer.config(dict(Buffer.__dict__))  # sets header
            Lbuffer.tBin = self.tStep
            #WXBS.config(wimax.getNumSymFr(self.tFr),self.ofdma)
            WXBS.nSymFr = Wimax.getNumSymFr(self.tFr[self.mac[0]])
        else:             
            self.configVBRload(self.tRate*self.tPoll*1e3/8)  # convert to bytes
            # first call tRate might be an array if multiparametes
        WXBS.config(init, self.pMax)
        
        
            
            # nNod differs between runs
        

       
        

    def __init__(self):

       WLnet.__init__(self)
       self.mode = expand_dims(self.mode,1) # add 2-nd dimension nBuf
       self.bs = WXBS()   # initially one bs
       
       for n in range(self.nNod):
           self.node.append(WXnode(n, self.bs.pid))
           sc = n % self.srvDist*self.nNod # threshold for sc = 0
           self.node[n].connSrc(sc)
           self.node[n].setup() # assigns nSrc and creates data array in C++
           if not self.ofdma:
               self.setMode(n)  # set transmision mode
         
        
       if self.ofdma:
       
           self.fch = Fchan(self.nNod, self.nFadeSample)
           self.sampleN = self.nFadeSample # triggers fading generation
           self.avLgain = self.lG.copy() # average link gain
           self.fade(0)
           
       
       self.poll(0)
       self.proceed = True
     
   
    
       
        
        
    def poll(self,t):
        # dbs()
        self.load(t)
        Event(t + self.tPoll,'poll',-1)
        # dbs()
     

    def fade(self,t):

        if self.sampleN == self.nFadeSample:  # generate new fade
            self.fch.generate()
            self.gain = reshape(self.fch.gain[:,32:224,:],(self.nNod,48,4,self.nFadeSample)).mean(2)
            # now for nFFT = 256 and nDC = 384, generalise later
            self.sampleN = 0

        
            
        if not self.amc:
            self.lG = self.avLgain + log10(self.gain[:,:,self.sampleN].mean(1))
            for n in range(self.nNod):
                self.setMode(n)
                self.bs.mode[n] = Wimax.nByteSlot[self.mode[n]]
         
                
        self.bs.prepare(self.gain[:,:,self.sampleN]*expand_dims(self.lGa,1))
        # dbs()    convert(opt["nByteSlot"],nByteSlot);
        
    
        
        
        
        self.sampleN += 1
        
        Event(t + Fchan.tSample,'fade',-1)
    


    def update(self, t):
        # dbs()
        if any(self.bs.grant):
            self.res.add('tr',self.bs.grant.sum()*1e-3) # kBytes
   ##          for n in range(self.nNod):
##                 for l in range(2):
##                     if self.bs.grant[n,l] > self.node[n].buf[l].sz:
##                          dbs()
                    # self.node[n].buf[l].remove(self.bs.grant[n,l], t)
            # dbs()       
            self.transmit(t, transpose(self.bs.grant))
            # transmit grant from the previous period
            
     
        # dbs()
        if any(self.bs.req):
            if any(self.bs.req < 0):
                dbs()
            self.bs.allocate()
           
       

    def stop(self, t):
        size = (self.nNod,2)
        delay = zeros(size)
        drop = zeros(size)

        for n in range(self.nNod):
            for k in range(2):
                _buf = self.node[n].buf[k]
                if _buf.rx:
                    delay[n,k] = _buf.delay/_buf.rx
                drop[n,k] = _buf.drop/_buf.load

        delay *= 1e3 # in ms
    
        fi = delay.sum(0)**2/(self.nNod*(delay**2).sum(0))
        # fairness index

        n = self.res.rN
        # dbs()
        if self.hist:
            stat = self.countHist()
            self.res.val['m_del'][n] = stat[0][0]*1e3
            self.res.val['std_del'][n] = stat[0][1]*1e3
        else:
            self.res.val['m_del'][n] = delay.mean(0)
            self.res.val['std_del'][n] = delay.std(0)

        self.res.val['fair'][n] = fi.mean(0)

        self.res.val['drop'][n] = drop.mean(0)

        self.res.val['tr'][n] /= t
        
        
        Thread.stop(self,t)
            

####################################  WXBS ########################################
# BS separated from net for possible future multicell extensions
        


        
    
                

        
        
class WXnode(cWXnode, WLnode):

    def __init__(self, id, bs):
        cWXnode.__init__(self, id, bs)        
        
        

   
        
        


      
      
          
          
          


      
      


