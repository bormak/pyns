#include "wxnet.hpp"
#include <buffer.hpp>
#include "float.h"


int WXBS::nSymFr = 0, WXBS::nBlPdu = 0, *WXBS::nByteSlot, WXBS::nNod = 0, 
  WXBS::nSubCh = 0, WXBS::nMode = 0;
double WXBS::noise = 0, *WXBS::pMax = 0, *WXBS::dEb = 0;
bool WXBS::wf = false, WXBS::pr = false, WXBS::mg = false, WXBS::fp = false, 
  WXBS::ei = false, WXBS::gp = false, WXBS::iu = false;





void WXBS::greedy(object sorted, object nbl) {
  // for fragmentation nBlPdu = 1

  // sorted is one-dimensional
  int rem, nod, lnk, nBlSym, nodReq, reqSym;  // remaining request
  int nSym = 0;
  int pos = 2*nBuf;  // position of buffer in sorted
  
  while (nSym < nSymFr) {
    nod = pos%nBuf;
    lnk = pos/nBuf;
    nBlSym = _i(nbl[nod]); // number of blocks per symbol
    nodReq = _i(req[nod][lnk]); // nod request
    reqSym = (int) ceil((nodReq*nBlPdu + 1)/nBlSym) + lnk;
    // requested symbols convert req in nBl or nPdu  to nSym, 
    // add 1 bl for header and 1 sym for uplink preamble
    rem = nSymFr - nSym;  // remaining number of symbols
    if (reqSym <= rem)
      grant[pos][pos/nBuf] = nodReq;
    else {
      grant[pos][pos/nBuf] = (int) floor(((rem - lnk)*nBlSym - 1)/nBlPdu);
      // get from rem granted nPdu, for fragmentation nBlPdu = 1
      reqSym = rem;
    }
    nSym += reqSym;
  }
}
  




bool WXBS::handle(std::string type, va_list ap) {

  if (!type.compare("req")) {
    int nod = va_arg(ap, int);
    int buf = va_arg(ap, int);    
    int sz = va_arg(ap, int);
    double delay = va_arg(ap, double);
    req[buf][nod] = sz;
    mdel[buf][nod] = delay;
    return true;
  }
  else
    return false;
}

 
void WXBS::config(dict opt, bool init) {
  if (init) {
    nSymFr = _i(opt["nSymFr"]);  // capacity
    nBlPdu = _i(opt["nBlPdu"]);  // number of blocks in PDU
    nSubCh = _i(opt["nSubCh"]);
    convert(opt["pMax"],pMax);
    convert(opt["nByteSlot"],nByteSlot);
    nMode = _i(opt["nMode"]);
    noise = _d(opt["noise"]);
    convert(opt["dEb"],dEb);	    
  }
  else { // parameters that can be changed between runs
    nNod  = _i(opt["nNod"]);
    wf = _b(opt["wf"]);
    pr = _b(opt["pr"]);
    iu = _b(opt["iu"]);
    fp = _b(opt["fp"]);  // fair proportional
    mg = _b(opt["mg"]);
    ei = _b(opt["ei"]);  // energy increment
    gp = _b(opt["gp"]);  // greedy power
  }  
}


void WXBS::assign(int lnk) {

  int n, k, nod, ch;
  bool cont = true;

  
  if (wf && !lnk)
    for (n = 0; n < nNod; n++)
      ((list) alloc[0]).append(list());

  if (fp) {
    double* acRate = new double[nNod];
    for (n = 0; n < nNod; n++)
      if (_i(active[lnk][n]))
	acRate[n] = 1;  // initial rate
      else
	acRate[n] = DBL_MAX;  // to eliminate it from assignment
    
    object iRate;   // incremented rate
    
    for (ch = 0; ch < nSubCh; ch++) {
      iRate = rate[lnk][ch]._f(copy)();
      for (n = 0; n < nNod; n++)
	iRate[n] += acRate[n];
      nod = _i((rate[lnk][ch]._f(__div__)(iRate))._f(argmax)());
      acRate[nod] += _d(rate[lnk][ch][nod]);
      assignChan(lnk, nod, ch);
    }
    delete acRate;
  }
	         

  else if (mg) {
    ch = 0;   
    while (active[lnk]._f(any)() && ch < nSubCh) {    
      k = 0;
      while (_i(free[_e(lnk,ch)]) && k < nNod) {
	nod = _i(srtGain[_e(k,ch)]);
	if (_i(active[_e(lnk,nod)]))
	  cont = assignChan(lnk, nod, ch);
	if (!cont)
	  break;
	k++;
      }
      ch++;
    }
  }
	    	 
  else if (iu) {
    n = 0;
    while(cont && free[lnk]._f(any)() && ch < nSubCh) {
      nod = _i(prior[_e(lnk,nod)]);
      k = 0;
      while (_i(active[_e(lnk,nod)]) && k < nSubCh) {
	ch = _i(srtGain[_e(nod,k)]);
	if (_i(free[_e(lnk,ch)]))
	  assignChan(lnk, nod, ch);
	if (!cont)
	  break;
	k++;
      }
      n++;
    }	  
  }

  else {
    k = 0;
    while (cont && free[lnk]._f(any)() && k < nSubCh) {
      for (n = 0; n < nNod; n++) {
	nod = _i(prior[_e(lnk,n)]);
	if (_i(active[_e(lnk,nod)])) {
	  ch = _i(srtGain[_e(nod,k)]);
	  if (_i(free[_e(lnk,ch)]))
	    cont = assignChan(lnk, nod, ch);
	}
		
	if (!cont)
	  break;
      }     
      k++;
    }
  }
    
}
		
    

bool WXBS::assignChan(int lnk, int nod, int ch) {
  
  if (!gp) {
    reserve(lnk, nod, ch); // mode = - 1, only reserve channel
    if (lnk) {
      int chN = _i(alloc[1]["nCh"]);
      alloc[1]["nod"][chN] = nod;
      alloc[1]["ch"][chN] = ch;
      alloc[1]["nCh"] += 1;
    }
    else 
      alloc[0][nod]._f(append)(ch);
    // of some reason ((list) alloc[0][nod]).append(ch) doesn't work
  }
  else {
    double _pT = lnk ? _d(pT[1]) : _d(pT[0][nod]);
    object _pTreq = pTreq[_e(nod,ch)];
    int mode = _i(_pTreq._f(searchsorted)(_pT));
    if (mode) {
      mode -= 1;
      if (lnk)
	pT[1] -= _pTreq[mode];
      else
	pT[0][nod] -= _pTreq[mode];
      reserve(lnk,nod,ch,mode);
    }
    else
      active[_e(lnk,nod)] = 0;
  }

  if (lnk && !_d(pT[1]))
    return false;
  else
    return true;
}


void WXBS::ulPow() {

  int nod, *mode, n, ch, nCh;
  object _gain, _pTreq;
  
  
  for (nod = 0; nod < nNod; nod++) {
      nCh = len(alloc[0][nod]);
      if (nCh) {
	_gain = gain[nod]._f(take)(list(alloc[0][nod]));
	_pTreq = pTreq[nod]._f(take)(list(alloc[0][nod]),0);
	//take elements from axis 0
	if (wf)
	  mode = wfill(_gain, _pTreq, nCh, pMax[0]);
	else
	  mode = greedyPow(_gain, _pTreq, nCh, pMax[0]);

	for (n = 0; n < nCh; n++)
	  if (mode[n] >= 0) {
	    ch = _i(alloc[0][nod][n]);
	    reserve(0, nod, ch, mode[n]);
	  }
	
	delete mode;
	alloc[0][nod] = list(); 
      }
  }
}

	    
void WXBS::dlPow(object _gain, object _pTreq) {

  int nCh, *mode,  n, nod, ch;

  nCh = _i(alloc[1]["nCh"]); 

  if (wf)
    mode = wfill(_gain, _pTreq, nCh, pMax[1]);
  else
    mode = greedyPow(_gain, _pTreq, nCh, pMax[1]);

  for (n = 0; n < nCh; n++)
    if (mode[n] >= 0) {
      nod = _i(alloc[1]["nod"][n]);
      ch = _i(alloc[1]["ch"][n]);
      reserve(1, nod, ch, mode[n]);
    }

  delete mode;
}
	
      
int* WXBS::greedyPow(object _gain, object _pTreq, int nCh, double _pMax) {

  int i, n;
  int* mode = new int[nCh];
  double pAlloc = 0;
  object srt = (_gain._f(__pow__)(-1))._f(argsort)();
  bool cont = true;
  
  for (n = 0; n < nCh; n++) {
    i = _i(srt[n]);
    if (cont) {   
      mode[i] = _i(_pTreq[i]._f(searchsorted)(_pMax - pAlloc)) - 1;

      if (mode[i] >= 0)
	pAlloc += _d(_pTreq[i][mode[i]]); // shorter than [_e(i,mode[i]]
      else 
	cont = false;
    }
    else
      mode[i] = -1;  // no need for searchsorted if power exhausted
  }
  return mode;
}
		    
	    
	    

int* WXBS::wfill(object _gain, object _pTreq, int nCh, double _pMax) {

  int n, ch;
  double* invSNR = new double[nCh];
  double* pAlloc = new double[nCh];
  double* pWF = new double[nCh];
  int* mode = new int[nCh];
  double invSNRsum = 0;
  object srt, _dEb;
 
  
  for (n = 0; n < nCh; n++) {
    invSNR[n] = noise/_d(_gain[n]);
    invSNRsum += invSNR[n];
  }
  
  double delta = (_pMax + invSNRsum)/nCh;  
  double pRes = 0;  
  object dP = _gain._f(copy)();
  dP._f(fill)(0);

  if (ei) {
    _dEb = _gain._f(copy)();
    _dEb._f(fill)(0);
  }
   
  for (n = 0; n < nCh; n++) {
    pWF[n] = delta - invSNR[n];
    pAlloc[n] = 0;
    if (pWF[n] < 0) {
      pWF[n] = 0;
      mode[n] = -1;
    }
    else {
      mode[n] = _i(_pTreq[n]._f(searchsorted)(pWF[n])) - 1;
      if (mode[n]) {
	// mode[n]--;
	pAlloc[n] = _d(_pTreq[_e(n, mode[n])]);
	if (mode[n] < nMode - 1) {
	  if (ei)
	    _dEb[n] = dEb[mode[n]]/_d(_gain[n]);
	  dP[n] = _d(_pTreq[_e(n, mode[n]+1)]) - pAlloc[n];
	}  
      }
    }
    pRes += pWF[n] - pAlloc[n];
  }

  if (dP._f(any)()) {
    if (ei)
      srt = _dEb._f(argsort)();
    else
      // inverse unnecessary for uplink if not fp, how to avoid it
      srt = (_gain._f(__pow__)(-1))._f(argsort) ();

    int last = _i( ((dP._f(take)(srt))._f(cumsum)())._f(searchsorted)(pRes));
   
    for (n = 0; n < last; n++) {
      ch = _i(srt[n]);
      if (mode[ch] < nMode - 1)
	mode[ch]++;
    }
  }

  delete pAlloc;
  delete pWF;
  delete invSNR;
  return mode;
}
      
    
    
	    


void WXBS::reserve(int lnk, int nod, int ch, int mode) {

  tuple str = _e(lnk,nod);
  
  

  int nBytes = 0;

  if (mode >= 0)
    nBytes = nByteSlot[mode];


  if (nBytes) {
    int _req = _i(req[str]);
    if (nBytes > _req)
      nBytes = _req;
 
    req[str] -= nBytes;
    grant[str] += nBytes;
    if (!_i(req[str]))
      active[str] = 0;
  }
    
  free[_e(lnk,ch)] = 0;

  if (pr) {
    grSlot[str] -= 1;
    if (!_i(grSlot[str]))
      active[str] = 0;
  }
}
    
    
  


 

   
   
  
 
    
		           
				

    

/////////////////// WXnode //////////////////////////////////////////////////



void WXnode::load(double time) {
   double delay = 0;  // mean delay per byte
  int nBytes = 0;  // buffer size in sdu and bytes 

  
  for (int n = 0; n < nSrc; n++) {  // nSrc = nBuf, 
     Lbuffer& _buf = extract<Lbuffer&>(buf[n]);
    
     _buf.input(time);
      
     if (_buf.size()) 
       delay = _buf.check(time);  // mean over frames
       // check removes overdelayed frames and calculates total delay
     
     else if (!_buf.isStream())
       src[n]._f(wait)(time);   
     // generates transit event and goes to Off state if was in On
     // precluding generation of new events
     
     if (nBytes = _buf.getSize())
       addr[bs]->signal("req", id, n, nBytes, delay); // current delay
     // for multicell, a bs pid should be given
  }
}


void WXnode::transmit(double time, object grant) {

  int data;
  for (int n = 0; n < nSrc; n++) {
    Lbuffer& _buf = extract<Lbuffer&>(buf[n]);
    data = _i(grant[n]);
    // assert(data <= _buf.getSize());
    if (data && _buf.getSize())       
      _buf.remove(data, time);
  }
}



BOOST_PYTHON_MODULE(libwxnet) {

  class_<WXBS, bases<Process> >("cWXBS")
    .def("greedy", &WXBS::greedy)
    .def("config", &WXBS::config)
    .staticmethod("config")
    .def("cReserve", &WXBS::reserve)
    .def("cAssign", &WXBS::assign)
    .def("cAssignChan", &WXBS::assignChan)
    .def("cUlPow", &WXBS::ulPow)
    .def("cDlPow", &WXBS::dlPow)
    .def_readwrite("req", &WXBS::req)
    .def_readwrite("mdel", &WXBS::mdel)
    .def_readwrite("grant", &WXBS::grant)
    .def_readwrite("nBuf", &WXBS::nBuf)
    .def_readwrite("srtGain", &WXBS::srtGain)
    .def_readwrite("gain", &WXBS::gain)
    .def_readwrite("free", &WXBS::free)
    .def_readwrite("pT", &WXBS::pT)
    .def_readwrite("grSlot", &WXBS::grSlot)
    .def_readwrite("active", &WXBS::active)
    .def_readwrite("prior", &WXBS::prior)
    .def_readwrite("pTreq", &WXBS::pTreq)
    .def_readwrite("alloc", &WXBS::alloc)
    .def_readwrite("rate", &WXBS::rate)
    ;

  class_<WXnode, bases<WLnode> >("cWXnode", init<int, int>())
      ;
}








      
      
    
  
  
	
      
    
