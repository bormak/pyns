
from wxNet import *
from wxbs import *
# named wxNet to distinguish from module name
# to allow import from wxnet directory





clp = '''WXnet.srvDist, 1, service distribution,--sdist
WXBS.alg,'', allocation algorithm, --alg
WXnet.mac, e_amc, mac, --mac
WXnet.tRate, 496, traffic mean rate, --tr
WXBS.nBlPdu, 3, number of blocks in pdu without header,--nbl
'''






# default values put in a function

def default():
    from itpy import Fchan


    WXnet.tFr = {'d' : 10e-3, 'e' : 5e-3}
    WXnet.tPoll = 41e-3  # one video frame 1 to be later than update
    
    # WXnet.tRate = 300  # in kbps
    # mean rate - 6*n blocks in frame,  2 - 18 
    WXBS.header = 8  # MAC pdu header in bytes
    WXBS.dlPmax = 25  # dl Power times ul power
    WXBS.preamble = 1 # one symbol
    WXBS.nBin = array([[1, 2],[6, 3]]) # frequency, time
    Buffer.header = 0 # fragmentation/packing subheader


    WLnode.service = [
        {'name' : 'video', 'srcType' : 'vbr', 'nSrc' : 2, 'srcClass' : 'Msource',
         'buf' : {'type' : 'Lbuffer', 'stream' : True , 'cap' : int(1e6), 'maxDelay' : 2,
                  'tBin' : 0, 'hist' : []}
         },
        {'name' : 'data', 'srcType' : 'pareto', 'nSrc' :  1, 'srcType' : 'Dsource',
         'buf' : {'type' : 'Lbuffer', 'isStream' : False , 'cap' : 100, 'maxDelay' : 5,
                  'tBin' : 100 }
         
        },
        ]

    Thread.src = {
        'vbr' : { 'dist' : 'n', 'nSt' : 5, 'tStMin' : .04},
        'pareto' : { 'fsz' : 1e3, 'alpha' : 1.8, 'tOff' : 30 },
        'trLogNormal' : {'minFsz' : 1e2, 'maxFsz': 1e4, 'sigma' : 1, 'tOff' : 30},
        'fixed' : { 'fsz' : 1e3, 'tOff' : 30 }
        }

    Fchan.chType = 'rbu'  # reduced bad urban
    Fchan.nFFT = 256 # must be chosen such that discrete delay profile is distinct
    Fchan.v = 1        #  1 m/s for Doppler
    Fchan.f0 = 3e9     # 3 gHz frequency
    Fchan.tSample = 20e-3

    WXnet.nFadeSample = 100  # number of fading samples generated at once

    WLnet.topo = 'line'
    WLnet.bw = 5e6
    WLnet.reqSNR = array([6.4, 9.4, 11.2, 16.4, 18.2, 22.7, 24.4])/10 # in Bells

    WLnet.pMax = 0.0  # in Bells
    WLnet.margin = 1.3
   
    Result.names = {'m_del' : 2,'std_del' : 2,'tr' : 2, 'drop' : 2,'fair' : 2}
   
  
    

threadClass = 'WXnet'
