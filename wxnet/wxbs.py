from core import *
# from wxnet import WXnet
from libwxnet import *

class WXBS(cWXBS):

    @classmethod
    def config(self, init, pMax):

        # pMax is given from WXnet cause of some reason WXnet can't be imported
        

        if init:
            self.nSubCh = Wimax.nSubCh # subCh or bin ?
            self.nByteSlot = int_(Wimax.nByteSlot)     
            if self.amc:
                self.nSlot = self.nSymFr/self.nBin[1][0] # how to account for different bins
                # number of slot per link in time domain
                self.dEb = diff(self.pR)/diff(self.nByteSlot)
                self.pR = self.pR.reshape((1,1,Wimax.nMode)) # for calculating pTreq
                
                self.pMaxSubCh = pMax/self.nSubCh
            else:
                self.nSlot = self.nSymFr/2

            self.nSlotFr = self.nSlot*self.nSubCh
            # number of slot per frame in both domains
            self.pMax = array([pMax, self.dlPmax*pMax])  # BS pMax is nNod times ms pMax
            self.noise = WLnet.aNoise  # to be accessible in Cpp
            self.nMode = Wimax.nMode   # the same reason
                  
            if Sim.type == 'py':
                self.ex = 'py' # execution
            else:
                self.ex = 'c'
        else:
            # called when alg and nNod is assigned
            self.nNod =  WLnet.nNod           
            self.wf = 'w' in self.alg   # water-fliing
            self.pr = 'p' in self.alg   # proportional
            self.iu = 'u' in self.alg   # iteration over users
            self.fp = 'f' in self.alg   # fair proportional
            self.mg = 'g' in self.alg   # assignment by maximal gain, 'n' - normalised
            self.ei = 'e' in self.alg  # ei sorting by energy increment for wf
            # default Wong or s
            if self.wf or self.mg or self.fp:
                self.gp = False  # greedy power
            else:
                self.gp = True  
            
            if self.fp:
                self.pAv = (self.pMax/self.nSubCh).reshape((2,1,1))
        
        cWXBS.config(dict(self.__dict__), init)

 

    def __init__(self):
        cWXBS.__init__(self)

        shape = (2, self.nNod)  # to allow easy access in cpp x[lnk]
        # also reduced space in dispaly
        
        self.req = zeros(shape,'int') # 'i' gives 'int32'
        self.mdel = zeros(shape)
        self.grant = zeros(shape, 'int')
        self.prior = empty(shape, 'int')

        if self.amc:
            
            # preallocate arrays, initialised every call to allocate
            self.free = zeros((2,self.nSubCh),'int') # 2 links
            self.active = ones(shape,'int')
            self.srtGain = zeros((self.nNod,self.nSubCh))
            if not self.gp:
                self.alloc = [[],{'nod' :-1*ones(self.nSubCh,'int'),
                                  'ch' : -1*ones(self.nSubCh,'int'), 'nCh' : 0}]
                for n in range(self.nNod):
                    self.alloc[0].append([])
                if self.fp:
                    self.rate = zeros((2,self.nSubCh,self.nNod))
           
        else:
            self.mode = zeros((self.nNod),'int')
        

        if self.pr:
                self.grSlot = zeros(shape,'int')
        # granted slots proportionally to requests

      ##   if self.wf:
##             self.alSlot = zeros((2,self.nNod,self.nSubCh),'int')
            # allocated slots for each subchannel after first step
       

    def prepare(self, gain):

        
         # greedy delay   

        if self.amc:
            self.gain = gain
            self.pTreq = self.pR/expand_dims(gain,2) # required power for all modes

            if self.mg:  # normalised gain
                if 'n' in self.alg:
                    _gain = self.gain/expand_dims(self.gain.mean(1),1)
                    # normalised to mean channel gain
                else:
                    _gain = self.gain
                    
                self.srtGain = flipud(_gain.argsort(0))
                # sorted gain for easy access in C++

            elif self.fp:
                _gain = expand_dims(self.gain.transpose(),0)
                self.rate = log(1 + self.pAv*_gain/self.noise)
                
                
            else:  # allocation by users or wong

                if 'd' in self.alg:
                    self.prior = flipud(self.mdel.argsort(1))
                elif 'm' in self.alg:  # max or min channel priority
                    mgain = self.gain.mean(1).argsort()
                    if 'a' in self.alg:   # max
                        mgain = flipud(mgain)
                    self.prior = expand_dims(mgain,0).repeat(2,0)
                else:
                    for n in range(2):
                        self.prior[n] = random.permutation(self.nNod)
                    # randomised priority
                # self.srtGain = map(flipud, [x.flatten().argsort() for x in vsplit(self.gain,self.nNod)])
                self.srtGain = fliplr(self.gain.argsort(1))

         
        else:
           if 'gd' in self.alg:
               weight = self.mdel**0*expand_dims(self.mode,1)
               self.prior = flipud(weight.argsort(0))
               

        
                  

       
        

          


    def allocate(self):
        
        if self.ofdma:
            # dbs()
            # calculate nSlotLnk
            totReq = double(self.req.sum(1))
            nSlotLnk = int_(round_(totReq/self.req.sum()*self.nSlot))
            # nSlotLnkTot = zeros((2),'int')

            if self.amc:
                self.grant[:] = 0  # granted bytes
                for lnk in range(2):
                    grant = zeros((2,self.nNod))                            
                    for slot in range(nSlotLnk[lnk]):
                        if slot > 1 and all(self.req[:,lnk]):
                            # assignment will be repeated, no need to run it
                            self.grant[lnk] += dGrant
                            self.req[lnk] -= dGrant
                            self.req[lnk, self.req[lnk] < 0] = 0
                        else:
                            if self.pr:
                                self.share(self.nSubCh, totReq[lnk], lnk)  #
                            self.free[lnk] = 1
                            self.active[lnk] = 1  
                            self.active[lnk, self.req[lnk] == 0] = 0 # also reset to 0 if pT or grSlot == 0
                            self.pT = [self.pMax[0]*ones(self.nNod), self.pMax[1]] # up- and downlink
                            # dbs()
                            exec 'self.%sAssign(lnk)' % self.ex
                            # self.pyAssign(lnk)
                            if not self.gp:
                                if lnk:
                                    nCh = self.alloc[1]['nCh']
                                    nodes = self.alloc[1]['nod'][0 : nCh]
                                    chan =  self.alloc[1]['ch'][0 : nCh]
                                    gain = self.gain[nodes, chan]
                                    pTreq = self.pTreq[nodes, chan]
                                    # dbs()
                                    # gain and pTreq needed in cpp, cause take() can not have complex index sequences [nod, ch]
                                    exec 'self.%sDlPow(gain,pTreq)' % self.ex
                                    self.alloc[1]['ch'][:] = -1
                                    self.alloc[1]['nod'][:] = -1
                                    self.alloc[1]['nCh'] = 0
                                else:
                                    # dbs()
                                    exec 'self.%sUlPow()' % self.ex                             

                        dGrant = self.grant[lnk] - grant[lnk]
                        grant[lnk] = self.grant[lnk]
                        
         
            else:
                # align shape later
                if self.pr:
                    for lnk in range(2):
                        self.share(nSlotLnk[lnk]/self.nSlot*self.nSlotFr, totReq[lnk], lnk)
                    self.grant = int_(minimum(self.grSlot*expand_dims(self.mode,1),self.req))
                else:
                    self.grant[:] = 0
                    for lnk in range(2):
                        avSlots = nSlotLnkTot[lnk]
                        n = 0
                        while avSlots and n < self.nNod:
                            nod = self.prior[n,lnk]
                            if self.req[nod,lnk]:
                                reqSlots = ceil(self.req[nod,lnk]/self.mode[nod])
                                grSlots = int(minimum(reqSlots, avSlots))
                                self.grant[nod,lnk] = minimum(grSlots*self.mode[nod],self.req[nod,lnk])
                                avSlots -= grSlots
                            n += 1
                            # required slots
            
                if any(self.grant > self.req):
                    dbs()
                self.req -= self.grant
                
                
                            
                        
            
               
            
          


    def pyAssign(self, lnk):
        
        cont = True  # indicator for downlink power

        if self.fp:
            acRate = ones(self.nNod) # accumulated rate
            acRate[self.active[lnk] == 0] = Inf
            for ch in range(self.nSubCh):
                k = (self.rate[lnk,ch]/(acRate + self.rate[lnk,ch])).argmax()
                acRate[k] += self.rate[lnk,ch,k]
                exec 'self.%sAssignChan(lnk, k, ch)' % self.ex
              
                
        
        if self.mg:
            ch = 0
            while cont and any(self.active[lnk]) and ch < self.nSubCh:
                k = 0
                while self.free[lnk,ch] and k < self.nNod:
                    nod = self.srtGain[k,ch]  # first priority
                    if self.active[lnk,nod]:
                        exec 'cont = self.%sAssignChan(lnk,nod,ch)' % self.ex
                    if not cont:
                        break # happens when pT[1] = 0
                    k += 1
                ch += 1
               
                    
                
        elif self.iu:
            n = 0
            while cont and any(self.free[lnk]) and n < self.nNod:
                nod = self.prior[lnk,n]
                k = 0  # subchannel
                while self.active[lnk,nod] and k < self.nSubCh:
                    ch = self.srtGain[nod,k]
                    if self.free[lnk,ch]:
                        exec 'cont = self.%sAssignChan(lnk,nod,ch)' % self.ex
                    if not cont:
                        break
                    k += 1
               
                n += 1
          

        else:   # wong
            k = 0
            while any(self.free[lnk]) and k < self.nSubCh:
                # dbs()
                for n in range(self.nNod):
                    nod = self.prior[lnk,n]
                    if self.active[lnk,nod]:
                        ch = self.srtGain[nod,k]
                        # k-th channel in sorted gain list 
                        if self.free[lnk,ch]:
                            exec 'cont = self.%sAssignChan(lnk,nod,ch)' % self.ex
                            # self.pyAssignChan(lnk,nod,ch)
                        if not cont:
                            break                     
                k += 1



        
                
                                     
                    
    def pyAssignChan(self,lnk, nod, ch):
    
       if not self.gp:
             # only reserve channel, for ng greedy loading can't be performedwith bin allocation
             # since the channels are not sorted, greedy cmpatible only with wong
             exec 'self.%sReserve(%d,%d,%d)' % (self.ex,lnk,nod,ch)
             if lnk:
                 chN = self.alloc[1]['nCh']  # channel number
                 self.alloc[1]['nod'][chN] = nod
                 self.alloc[1]['ch'][chN] = ch
                 self.alloc[1]['nCh'] += 1
             else:
                self.alloc[0][nod].append(ch)            
       else:
         
           if lnk:
               pT = self.pT[1]
           else:
               pT = self.pT[0][nod]

           pTreq = self.pTreq[nod,ch]

           mode = pTreq.searchsorted(pT)
           if mode:
               mode -= 1
               if lnk:
                   self.pT[1] -= pTreq[mode]
               else:
                   self.pT[0][nod] -= pTreq[mode]
               exec 'self.%sReserve(%d,%d,%d,%d)' % (self.ex,lnk,nod,ch,mode)

           else:
               self.active[lnk,nod] = 0

       if lnk and self.pT[1] == 0:
          return False
       else:
          return True
     
               
                                  
                            
                                        
                                    
                                
    def pyUlPow(self):

      for nod in range(self.nNod):
          nCh = len(self.alloc[0][nod])
          if nCh:                             
               gain = self.gain[nod,self.alloc[0][nod]]
               pTreq = self.pTreq[nod,self.alloc[0][nod]]
               
               if self.wf:
                   mode = self.pyWfill(gain, pTreq, nCh, self.pMax[0])
               else:  # fp
                   mode = self.pyGreedyPow(gain, pTreq, nCh, self.pMax[0])
               # dbs()
               for n in range(nCh):
                   if mode[n] >= 0:
                       ch = self.alloc[0][nod][n]
                       self.pyReserve(0, nod, ch, mode[n])

               self.alloc[0][nod] = []  # reset
       
       
    def pyDlPow(self, gain, pTreq):
       nCh = self.alloc[1]['nCh']

       if self.wf:
           mode = self.pyWfill(gain, pTreq, nCh, self.pMax[1])
       else:
           mode = self.pyGreedyPow(gain, pTreq, nCh, self.pMax[1])
           
       
       for n in range(nCh):
           if mode[n] >= 0:
               nod = self.alloc[1]['nod'][n]
               ch =  self.alloc[1]['ch'][n]
               self.pyReserve(1, nod, ch, mode[n])
        
                          
               
                                
       
           
       
                
            
    def pyWfill(self, gain, pTreq, nCh, pMax):
        
        invSNR = self.noise/gain
        delta = (pMax + invSNR.sum())/nCh

        pWF =  delta - invSNR
        pWF[pWF < 0] = 0
        mode = -1*ones(nCh,'int')  # int* in cpp
        pAlloc = zeros(nCh)      # double* in cpp
        dEb = zeros(nCh)         # gain.copy().fill(0) in cpp
        dP = zeros(nCh)
        for n in range(nCh):
            if pWF[n]:
                mode[n] = pTreq[n].searchsorted(pWF[n])
                if mode[n]:
                    mode[n] -= 1
                    pAlloc[n] = pTreq[n, mode[n]]
                    if mode[n] < self.nMode - 1:
                        if self.ei: # minimum energy increment
                            dEb[n] = self.dEb[mode[n]]/gain[n]
                        dP[n] = pTreq[n, mode[n] + 1] - pAlloc[n]

        # dbs()
        if any(dP):
             pRes = pWF.sum() - pAlloc.sum()
             if self.ei:
                 srt = dEb.argsort()
             else:
                 srt = flipud(gain.argsort())
                 
             last = dP.take(srt).cumsum().searchsorted(pRes)
             left = srt[0 : last]
             left = left.compress(mode[left] != self.nMode - 1)
             # pAlloc[left] += dP[left]
             mode[left] += 1
    
        # dbs()
        return mode
      
    
    def pyGreedyPow(self, gain, pTreq, nCh, pMax):
        
        srt = flipud(gain.argsort())
        pAlloc = 0
        mode = -1*ones(nCh,'int')
        for n in range(nCh):
            i = srt[n]
            mode[i] = pTreq[i].searchsorted(pMax - pAlloc)

            if mode[i]:
                mode[i] -= 1
                pAlloc += pTreq[i,mode[i]]
            else:
                #dbs()
                break
        return mode
                
 


    def pyReserve(self, lnk, nod, ch, mode = -1):

        str = (lnk,nod)  # stream
        if mode >= 0:
            nBytes = self.nByteSlot[mode]
        else:
            nBytes = 0
        
        if nBytes:

            if nBytes > self.req[str]:
                nBytes = self.req[str]

            self.req[str] -= nBytes
            self.grant[str] += nBytes
            
            if self.req[str] == 0:
                self.active[str] = 0

        self.free[lnk, ch] = 0


        if self.pr:
            self.grSlot[str] -= 1
            if self.grSlot[str] == 0:
                self.active[str] = 0

        
      
       
                
    def share(self,nSlot,totReq,n):
        self.grSlot[n] = round_(self.req[n]/totReq*nSlot)

    
        if self.grSlot[n].sum() > nSlot:
            srt = flipud(self.grSlot[n].argsort())
            k = 0
            while self.grSlot[n].sum() > nSlot:
                self.grSlot[n,srt[k]] -= 1
                k += 1
                    

        



                    
                            
