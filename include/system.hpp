#ifndef SYSTEM_H
#define SYSTEM_H

#include <queue>
#include <boost.hpp>
#include <vector>
#include <rand.hpp>

const int nProcMax = 10000;

// enum Signal {SOURCE, POINT};  // more signals to be added
// enum EvType {STOP, STEP, SAMPLE};
// signal and event types replaced by char* -> no need to specify and extend their enums



class Event;


class Scheduler : public std::priority_queue<class Event> {
public :
  Scheduler();
  Event  next();
  void clear();
};

///////////////////// Event //////////////////////////

class Event {
public :
  Event(double time, str evMsg, int genPid = -1) : 
    t(time), msg(evMsg), pid(genPid)  {
     sched->push(*this);  // -1 is Python
  } 

  Event(double time, int genPid) : t(time), pid(genPid) {
    sched->push(*this);
  }
  
  static void bind(class Scheduler* sched_) {
    sched = sched_;
  }
  
  bool react();
 

  bool python() {
    return pid == -1; // Python assigns -1 to pid
  } 
  
  double getTime() {
    return t;
  }
  
  int getPid() {
    return pid;
  }
  
  str getMsg() {
    return msg;
  }

  friend bool operator < (const Event& ev1, const Event& ev2) {
	 return ev2.t < ev1.t;
  }  // used for scheduler heap sorting

protected :
  double t;
  int    pid;  // requestor or generator pid
  str    msg; // event type or Python command
  static Scheduler* sched;
};


//////////////// Process /////////////////////////

/* Process is a class capable to react to events and signals
   process with pid 0, is the simulation thread (master process),
   containing simulation results and scheduler ? */

 
class Process {
public :
  Process() {
    pid = nProc;
    addr[nProc++] = this;
  }

  static Process* getAddr(int _pid) {
    return addr[_pid];
  }
  static Rnd* getRnd(object rand);
  virtual bool react(Event& ev) {
    return react(ev.getTime());
  }
  virtual bool react(double time) {
    return true;
  }
  // virtual void react(Process* client) {};
  void signal(std::string type, ...);
  // signal handling function, variable length arguments
  virtual bool handle(std::string type, va_list) { 
    return false;
  }

  virtual ~Process() {
    addr[pid] = NULL;       // to avoid using dead processes
  };
 int pid;  // process id
protected:
  static int nProc;  // number of processes
  static std::vector<Process*> addr;
};




///////////////////  Result   ////////////////////////////

struct Result {
  void set(const char* key, double value, int type = -1) {
    if (type == -1)
      val[key][rN] = value;
    else
      val[key][rN][type] = value;
  }
    
  void add(const char* key, double value, int type = -1) {
    if (type == -1)
      val[key][rN] += value;
    else
      val[key][rN][type] += value;
  }
//   void set(const char* key, double value) {
//     val[key][rN] = value;
//   }
//   void add(const char* key, double value) {
//     val[key][rN] += value;
//   }
  int rN;   // run number 
  dict val;
};

BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(Result_set_member_overload, set,2,3)
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(Result_add_member_overload, add,2,3) 
// of some reason boost default arguments don't work


struct Trace { 
  void sample(dict res);
  dict val;
  int sN;   // sample number
  int nTr;  // number of traces, for iteration in sample
};
    




class Thread : public Process {
public :
  virtual void start(std::string simType); 
  static void config(dict options); 
  
  static void setResult(object result) {
    res = result;
  }
  // these methods are needed as it ain't known how to 
  // expose directly static members to Python
  // static void setTrace(object tr) {
//     trace = tr;
//   }
  
//   static object getTrace() {
//     return trace;
//   }

  static void bind(Scheduler* scheduler) {
    sched = scheduler;
  }
  
  virtual bool run();
  virtual void step(double time) {
     new Event(time + tStep, "step", 0);
  }
  // boost can not create a wrapper for pure virtual functions
  virtual void stop(double time) {
    sched->clear();
  }
  virtual void sample(double time) {;}

  virtual void finish() {;}
  virtual bool react(Event& ev);
  
  void setResVal(const char* key, double val, int type = -1) {
    res._f(set)(key, val,type);
  }
  
  void addResVal(const char* key, double val, int type = -1) {
    res._f(add)(key, val,type);
  }

  object trace;
  double t; // simulation time
protected :
  static object res; 
  // static list rand; // dictionary
  // res is object, as it is not known yet how to assign extracted Result
  static Scheduler* sched;
  static double tStop;
  static double tStep;
  static double tSample;
};


#endif
