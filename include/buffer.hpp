#ifndef BUFFER_HPP
#define BUFFER_HPP

#include <boost.hpp>
#include <deque>

struct SDU {   // service data unit
  SDU(int sduSz, double time, int srcId, int destId = 0) : 
    t(time), sz(sduSz),  rem(sduSz), src(srcId), dest(destId) {};
  double t;  
  int sz; // sdu size in pdu's
  int rem; // remainder
  int src; // source id, needed for multi-hop networks
  int dest;
};

// Unlimited buffer, primarily used for collecting delay statistics

class Buffer : public std::deque<SDU> {
public : 
  Buffer(int nodeId, int dest = 0); 
  // input data > 0 if a protocol is used above 
  virtual void input(double time, double data = 0, int dest = 0);
  int getData() {
    return _data;
  }
  void setData(double data) {
    _data = (int) ceil(data);
  }
  void setDest(int dest) {
    _dest = dest;
  }
  int getSize() {
    return sz + size()*header;  // add optional fragmentation,packing headers
  }
  void remove(int data, double t);
  void content(); // show content
  virtual void add(SDU sdu) {
    push_back(sdu);
    sz += sdu.sz;
  }
  static void config(dict opt) {
    // pduSz = _d(opt["pduSz"]);
    header = _i(opt["header"]);
  }
  
  object repr();
  ~Buffer() {
    clear();
  }
  // statistics visible from Python
  
  double rx; // received packets
  double del; // delay statistics
protected:
  virtual void storeDelay(double dt, int data) { // nFrames
    // assert(dt > 0);
    rx += data;
    del += data*dt;
  }

  virtual int send(int data) {
    return data;
  } // derived send can have dropped PDUs
 
  int sz;  // size of buffer in pdu's
  static int header;  // pdu size
  int id;    // node id
  int _data;  // should data be integer ???
  int    _dest;
};

// limited buffer with service specific capacity,
// delay thresholds and histogram - ndpy array
// stream and burst input interface

class Lbuffer : public Buffer {
public :
  Lbuffer(int nodeId, dict opt, int dest = 0);
  void add(SDU sdu);
  double check(double time);
  void input(double time, double data = 0, int dest = 0) {
    Buffer::input(time, data, dest);
    if (!stream)
      _data = 0;  // precludes calling input again for nrt
  }
  
  bool isStream() {
    return stream;
  }

  // statistics visible from Python 
  double drop; // drop statistics
  double load; // needed to calculate drop fraction 

protected:
  bool  stream; // flow control for non real-time services
  void storeDelay(double t, int nFr);
  // counts delay of all sdu's and drops sdu's over delThr  

  double maxDelay; // max delay. after which packets are dropped
  int cap; // capacity in frames
  object hist; 
  int histSz;
  // common for all buffers created in Python
  double tBin; 
};


#endif
