#ifndef WLNET_HPP
#define WLNET_HPP

#include "system.hpp"


const int Conflict = -100;

class WLnet : public Thread {
public :
  void rxMesh(object transmitters, object receiversv);  
  static void config(dict opt);
  void transmit(double time, object grant);
  void load(double time);
 
  // calculate snr from transmitters to receivers for mesh (ad hoc) network
  object lG; // logarithmic link gain
  object lGa; // absolute link gain
  object pT; // logarithmic transmitted power
  object pTa; // absolute transmitted power
  object   pR; // received power from any transmitter to any receiver
  object   pRa; // absolute received power for scheduled transmitter, receiver
  object pN; // noise power N-N matrix (jamming included)
  object pNa; // absolute noise power
  object rxSNR; // received snr
  list node;  // nodes
  
protected:
  double getNoise(tuple link);
  static int nNod; 
  static int beam;
};

// wireless node

class WLnode : public Process {
public :
  WLnode(int nodeId) : id(nodeId) {};
  virtual void transmit(double time, object grant) {};
  virtual void load(double time) {};
  virtual void setup() {
    data = new double[nSrc];
  }
  bool handle(std::string type, va_list ap); // virtual in process
  list src;   // sources
  list buf;  // buffers
  int nSrc;
  int id;
protected:
  double* data;
};

  
  

#endif
  
  
      

			      
    
   
