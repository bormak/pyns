#ifndef BOOST_HPP
#define BOOST_HPP


#include <boost/python/tuple.hpp>
#include <boost/python/numeric.hpp>
#include <boost/python/list.hpp>
#include <boost/python/dict.hpp>
#include <boost/python/str.hpp>

#include <boost/python/object.hpp>
#include <boost/python/class.hpp>
#include <boost/python/def.hpp>
#include <boost/python/module.hpp>
#include <boost/python/overloads.hpp>
#include <boost/python/slice.hpp>

// latest macros

#define _f(x) attr(#x)  // function
#define _a(x) attr(#x)  // attribute

// for compatibility with old code

// #define f(x) attr(#x)  
// macros that can conflict to variables in external libraries like itpp
// #define a(x) attr(#x)  // attribute



using namespace boost::python;

// replace ed -> d ei -> i el -> e, edict-> dct elist ->lst

// new one-letter functions

inline bool _b(object o) {
  return extract<bool>(o);
}

inline double _d(object o) {
  return extract<double>(o);
}

inline int _i(object o) {
  return extract<int>(o);
}

// for compatibility with old code

inline bool b(object o) {
  return extract<bool>(o);
}

inline double d(object o) {
  return extract<double>(o);
}

inline int i(object o) {
  return extract<int>(o);
}

inline list lst(object o) {
  return extract<list>(o);
}

inline dict dct(object o) {
  return extract<dict>(o);
}

inline tuple _e(int i, int j) {
  return make_tuple(i,j);
}

inline tuple _e(int i, int j, int k) {
  return make_tuple(i,j,k);
}

inline tuple _e(int i, int j, int k, int n) {
  return make_tuple(i,j,k,n);
}

// for compatibility with old code

inline tuple e(int i, int j) {
  return make_tuple(i,j);
}

inline tuple e(int i, int j, int k) {
  return make_tuple(i,j,k);
}

inline tuple e(int i, int j, int k, int n) {
  return make_tuple(i,j,k,n);
}
// old two-letter functions

inline double ed(object o) {
  return extract<double>(o);
}

inline int ei(object o) {
  return extract<int>(o);
}
// used when i is variable
inline list elist(object o) {
  return extract<list>(o);
}

inline dict edict(object o) {
  return extract<dict>(o);
}

inline tuple el(int i, int j) {
  return make_tuple(i,j);
}

inline tuple el(int i, int j, int k) {
  return make_tuple(i,j,k);
}

inline tuple el(int i, int j, int k, int n) {
  return make_tuple(i,j,k,n);
}

/* inline int len(object o) {
  return ei(o.attr("__len__")());
}
*/

inline bool include(object x, char s) {
   return _b(x._f(__contains__)(s));
}


// creates array in C++ and fills it with values of python object
template<typename Type> int convert(object var, Type*& val) {
  int length = len(var);
  val = new Type[length];
    for (int i = 0; i < length; i++)
      val[i] = extract<Type>(var[i]);
    return length;
}
  

// converts opt[key] to val and return length of opt[key]
// used in passing dictionaries from python to C++
template<typename Type> int convert(dict opt, std::string key, Type*& val) {
  if (opt.has_key(key))
    return convert(opt[key], val);
  else
    return 0;
}


// allocate memory and init to zeros C++ calloc
template <typename Type> void zeros(Type* &x, int size)
{
	x = new Type[size];
	memset(x, 0, size*sizeof(Type));
}
// zeros takes pointers

  


typedef numeric::array Array; 
// for use of some numeric::array functions

// class ndarray : public object {
// public:
//   ndarray() : object(0) {;}
//   ndarray argsort(int axis=-1) {
//     return extract<ndarray&>(attr("argsort")(axis));
//   }
//   object sum() {          // how to enter None in boost
//     return attr("sum")();
//   }
//   object sum(int axis) {
//     return attr("sum")(axis);
//   }
// };

#endif
