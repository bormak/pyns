#ifndef SOURCE_HPP
#define SOURCE_HPP

#include <rand.hpp>
#include "system.hpp"
#include <boost.hpp>


  
// generic source, can be used for CBR

class Source : public Process {
public :
  Source(int nodePid, int srcId, dict parameters) : 
    node(nodePid), id(srcId), par(parameters) {};
  virtual void start(double time); // start not in constructor cause it calls virtual functions
  virtual void initState() {
    st = rnd->get() > .5 ? 1 : 0;  // for 2-state source
  }    
  virtual object __repr__();
   
  static void setRnd(object rand) {
     rnd = getRnd(rand);
  }
  bool react(double time);
  virtual double getLoad() {
    return _d(par["load"]);
  }
protected :
  virtual void transit() {};
  virtual void schedule(double t) {};
  virtual void notify() {
    addr[node]->signal("src", id, getLoad()); // node can access data from stored id
  }
  virtual void ch() {};
  dict par;
  static Rnd* rnd;
  int st;   // decalred here for __repr
  int node;
  int id;   // to distinguish between sources of one node
};




// generic exponential on-off source

class Esource : public Source {
public:
  Esource(int nodePid, int srcId, dict par) : Source(nodePid, srcId, par) {;}
  
protected:
  virtual void transit() {
    st = 1 - st;  //  on-off source
  }
  double getLoad() {
    return _d(par["load"][st]);
  }
  void schedule(double t) {
    Event(t + _d(par["tStMin"]) + rnd->exp(_d(par["tSt"][st])), pid);
  }
};

// Markov VBR source

class Msource : public Esource {
public: 
  Msource(int nodePid, int srcId, dict par) : Esource(nodePid, srcId, par) {;}
  void initState() {
      st = rnd->get(_i(par["nSt"])); 
  } 
protected: 
  void transit() {
     st = _i(par["trMat"][st]._f(searchsorted)(rnd->get()));

  }
};

// Data Source: on - sending data, off waiting

class Dsource : public Source {
public : 
  Dsource(int nodePid, int srcId, dict par) : Source(nodePid, srcId, par) {;}
   
  void start(double t) {
    st == 1 ? notify() : schedule(t);
  }
  bool react(double time) {  
    notify();
    st = 1;
  }
  void wait(double t) {
    if (st)
      schedule(t);
    st = 0;
  }      
protected:
  double getLoad();
  void schedule(double t) {
    Event(t + _d(par["tStMin"]) + rnd->exp(_d(par["tOff"])), pid);
  }
};



#endif


  
    
    



