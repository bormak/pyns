#ifndef ITPY_HPP
#define ITPY_HPP

#include <boost.hpp>
#include <itpp/itcomm.h>

namespace it = itpp;



it::cvec ar2cvec(object in);
it::cmat ar2cmat(object in);

void cvec2ar(it::cvec in, object out);
void cmat2ar(it::cmat in, object out);  


template<typename Type> it::Vec<Type> ar2vec(object in) {
 it::Vec<Type> out;
 int sz = _i(in._a(size));
 out.set_size(sz,false);
 for (int i = 0; i < sz; i++)
     out(i) = extract<Type>(in[i]);
 return out;
}

template<typename Type> void vec2ar(it::Vec<Type> in, object out) {
  // write exeption if sizes are not equal
  // object must be of matching numpy type 
  for (int i = 0; i < in.size(); i++)
    out[i] = in(i);
}

template<typename Type> it::Mat<Type> ar2mat(object in) {
  
  it::Mat<Type> out;
  object sh = in._a(shape);  // tuple
  int rows = _i(sh[0]);
  int cols = _i(sh[1]);
  out.set_size(rows,cols,false);
  for (int r=0; r < rows; r++)
    for (int c = 0; c < cols; c++)
      out(r,c) = extract<Type>(in[_e(r,c)]);
    
  return out;
}

template<typename Type> void mat2ar(it::Mat<Type> in, object out) {
  // write exeption if sizes are not equal
  for (int r = 0; r < in.rows(); r++)
    for (int c = 0; c < in.cols(); c++)
      out[_e(r,c)] = in(r,c);
}




class Fchan {
public :
  Fchan(dict opt);  // parameters of Fchan given as dict of the wrapping python object
  int genFreqGain(object gain); // returns abs(H) in gain
protected:
  it::TDL_Channel ch;
  int nFFT;
};

#endif
