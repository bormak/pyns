#ifndef RAND_HPP
#define RAND_HPP

#include<cmath>
#include<deque>

using namespace std;

const int NTAB = 32;  // random table

class Rnd {
public :
  void seed(long num);
  double operator() () { 
    return get(); 
  }
  long operator() (int range) { 
    return  get(range);
  }
  double exp (double lambda) { // exponential
    return -lambda*log(get());
  }
  double pareto (double x, double alpha) {
    return x/pow(get(),1/alpha);
  }
  double get();
  int get(int range) {
    return (int) floor(get() * range);
  }
private :
  long idum;
  long iy;
  long iv[NTAB];
  static const int	IA, IR;
  static const long	IM, IQ;
  static const double AM; 
  static const long NDIV;
  static const double EPS;
  static const double RNMX;
  void next();
};

class Rand : public deque<Rnd*> {
public :
  void seed(long seed_); 
  Rnd* get(); 
};  


#endif
