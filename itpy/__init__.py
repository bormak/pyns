from libitpy import *
from scipy import *

class Fchan(cFchan):
    '''
    First version of wrapped fading channel, tailorized for OFDM
    parameters to be defined before calling config
    power - power profile
    delay - delay profile in us
    default - reduced typical urban
    v - velocity, all links assume so far the same velocity
    f0 - frequency
    doppler - normalised doppler frequency = v/c*f0*tSample
    nFFT - number of FFT points
    tSym - OFDM symbol time, for delay sampling in numbers of tSym/nFFT
    returns tSample
    '''
    chProf = { 'rtu' :
               [[0, .2, .5, 1.6, 2.3, 5], [.189, .379, .239, .095, .061, .037]],
               'rbu' :
               [[0, .3, 1, 1.6, 5, 6.6], [.164, .293, .147, .094, .185, .117]]
               }
               
    
    chType = 'rtu'  # reduced typical urban
    
    @classmethod
    def config(self, rand):
        # discretise the delay according to tSample and sets normalised Doppler
        c = 3e8 # speed of light
        
        tDelSample = self.tSym/self.nFFT*1e6  # in us
        # nFFT must be selected such that self.delay is distinct 
        prof = self.chProf[self.chType]
        self.delay = int_(ceil(array(prof[0])/tDelSample))
        self.power = array(prof[1])
        attr = dict(self.__dict__)
        if attr.has_key('doppler'): 
            self.tSample = self.doppler*c/(self.f0*self.v)
        # normalised doppler is given, calculate tSample
        elif attr.has_key('tSample'):
            self.doppler = self.v/c*self.f0*self.tSample
        # tSample is given to calculate doppler
        rand.append(ITrnd())

    def __init__(self, nLink, nSample):
        self.gain = zeros((nLink, self.nFFT, nSample))
        cFchan.__init__(self, dict(Fchan.__dict__))

    def generate(self):
        self.genFreqGain(self.gain)
