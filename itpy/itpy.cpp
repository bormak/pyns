#include <itpy.hpp>

it::cvec ar2cvec(object in) {
    it::cvec out;
    int sz = _i(in._a(size));

    out.set_size(sz,false);

    for (int i = 0; i < sz; i++)
      out(i) = std::complex<double>(_d(in[i]._a(real)),_d(in[i]._a(imag)));
    return out;
}

void cvec2ar(it::cvec in, object out) {
  // write exeption if sizes are not equal
  for (int i = 0; i < in.size(); i++)
      out[i] = make_tuple((double) in(i).real(), (double) in(i).imag());
}


it::cmat ar2cmat(object in) {
  it::cmat out;
  object el;  // element in in
  object sh = in._a(shape);  // tuple
  int rows = _i(sh[0]);
  int cols = _i(sh[1]);
  out.set_size(rows,cols,false);
  for (int r=0; r < rows; r++)
    for (int c = 0; c < cols; c++) {
      el = in[_e(r,c)];
      out(r,c) = std::complex<double>(_d(el._a(real)),_d(el._a(imag)));
    }
  return out;
}







void cmat2ar(it::cmat in, object out) {
  // write exeption if sizes are not equal
  for (int r = 0; r < in.rows(); r++)
    for (int c = 0; c < in.cols(); c++)
      out[_e(r,c)] = make_tuple((double) in(r,c).real(), (double) in(r,c).imag());
}



Fchan::Fchan(dict opt) {
  it::vec pow = ar2vec<double>(opt["power"]); 
  it::ivec del = ar2vec<int>(opt["delay"]); // should be sampled
  ch.set_channel_profile(pow, del);
  ch.set_norm_doppler(_d(opt["doppler"]));
  nFFT = _i(opt["nFFT"]);
}



int Fchan::genFreqGain(object gain) {
  it::cmat coeff, resp;  
  object sh = gain._a(shape); // shape = (nLink, nFFT, nSample)
  // var name shape can't be used as it confuses with shape attribute of numpy
  int nL = _i(sh[0]);
  int nS = _i(sh[2]); // number of samples
  
  
  if (_i(sh[1]) != nFFT) {
    std::cout << "delay profile was not sampled to this nFFT \n";
    return 0;
  }
      

  for (int n = 0; n < nL; n++) {
    ch.generate(nS, coeff);
    ch.calc_frequency_response(coeff, resp, nFFT);
    mat2ar<double>(abs(resp),gain[n]); // assigns abs gain [nFFT,nS] to gain
  }    
  
  return 1;

}


BOOST_PYTHON_MODULE(libitpy) {
  class_<Fchan>("cFchan", init<dict>())
    .def("genFreqGain", &Fchan::genFreqGain)
    ;

  void (it::Random_Generator::*seed)(unsigned int) = &it::Random_Generator::reset;
  // recepie from the boost web site for the overloaded members
  // reset is overloaded, declare seed as pointer to reset(unsigned int) 
  
  class_<it::Random_Generator>("ITrnd")
    .def("seed", seed)
    ;
}
