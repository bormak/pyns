from numpy import *
from libitpy import *
from libpyns import Rand

class Fchan(cFchan):
    '''
    First version of wrapped fading channel, tailorized for OFDM
    parameters to be defined before calling config
    power - power profile
    delay - delay profile in us
    v - velocity, all links assume so far the same velocity
    f0 - frequency
    doppler - normalised doppler frequency = v/c*f0*tSample
    nFFT - number of FFT points
    tSym - OFDM symbol time, for delay sampling in numbers of tSym/nFFT
    returns tSample
    '''
    
    
    @classmethod
    def config(self, rand):
        # discretise the delay according to tSample and sets normalised Doppler
        c = 3e8 # speed of light
        
        tDelSample = self.tSym/self.nFFT
        self.delay = int_(ceil(self.delay/tDelSample))
        attr = dict(self.__dict__)
        if attr.has_key('doppler'): 
            self.tSample = self.doppler*c/(self.f0*self.v)
        # normalised doppler is given, calculate tSample
        elif attr.has_key('tSample'):
            self.doppler = self.v/c*self.f0*self.tSample
        # tSample is given calculate doppler
        rand.append(ITrnd())

    def __init__(self, nLink, nSample):
        self.gain = zeros((nLink, self.nFFT, nSample))
        cFchan.__init__(self, dict(Fchan.__dict__))

    def generate(self):
        self.genFreqGain(self.gain)
    

                       






def fchTest(nU, nS, nFFT):
    Fchan.power = array([.189, .379, .239, .095, .061, .037])
    Fchan.delay = array([0, .2, .5, 1.6, 2.3, 5.0]);
    Fchan.tSym = 13.7
    Fchan.nFFT = nFFT
    Fchan.v = 5  # 5 m/s - 18 km/h
    Fchan.f0 = 3e9
    Fchan.doppler = .1

    rand = list()
    Fchan.config(rand)
    rand[0].seed(2006)
    ch = Fchan(nU, nS);
    ch.generate()
    x1 = ch.gain
    ch.generate
    x2 = ch.gain
    return concatenate((x1,x2),2)
