from pylab import *
from numpy import r_
from core import loadResult



mark = ['rs-','go-', 'bx-', 'kv-', 'cd-','m^-','r>-']  
modes = ('g','w','wp') # 
vars = {
    'w' : ('m_del','std_del','fair'),
    'p' : ('m_del','std_del','fair')
    }
link = ('Uplink','Downlink')

# pLegend = ['a','b','c','d','e']


yLabel = ['mean delay',"delay's standard deviation",'fairness index']

meanDelMax = {'p' : 220, 'w' : 250}
stdDelMax = {'p' : 20, 'w' : 250}
nPlot = {'w' : 1, 'p' : 1}
xMax = 500




def draw(mode,file, sz = 0):

    if mode == 'all':
        for n in range(len(modes)):
            draw(modes[n],file)
    else:
        file = mode + file
        r = loadResult('wxsim',file)
        
        #loadStr = r['argv'][r['argv'].index('--tr') + 1] # load string
        #exec 'load = r_[%s]' % loadStr

        var = vars[mode]

        nSubPlot = len(var)
        nPl = nPlot[mode]

        if sz == 0:
            sz = [5*nPl, 3*nSubPlot] # in inches
    
        x = r['x']

        rc('font', size = 8, weight = 'bold')
        rc('lines', markersize = 5)
        rc('lines', linewidth = 1.5)
        rc('axes', labelsize = 8)
        rc('xtick',labelsize = 8)
        rc('ytick',labelsize = 8)
        rc('legend', fontsize = 8)

        ioff()

        figure(figsize = sz)
               
        for n in range(nPl):           
            for k in range(nSubPlot):
                sp = k*nPl + n + 1 
            
                ax = subplot(nSubPlot, nPl, sp)
                metric = r[var[k]]
                i = 0
                for key,y in metric.items():
                    if key != 'pm':
                        plot(x,y[:,n],mark[i])
                    i += 1

                if ax.is_first_row():
                    title(link[n])
                    axis([x[0], xMax , 0, meanDelMax[mode]])
                    if metric.has_key('pm'):
                        keys = metric.keys()[:-1]
                    else:
                        keys = metric.keys()
                    if ax.is_first_col():
                        legend(keys,loc = 2,prop = matplotlib.font_manager.FontProperties(size=9))

                if var[k] == 'std_del' and n == 0:
                    axis([x[0], xMax , 0, stdDelMax[mode]])

                if ax.is_last_row():
                    xlabel('mean load in kbps')

                if ax.is_first_col():
                    ylabel(yLabel[k])
                
                       
                grid(True)
                xticks(x)
                
            
            show()
       
    













if __name__ == '__main__':
    draw(mode,file,sz = 0)
