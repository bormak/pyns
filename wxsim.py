#!/usr/bin/env python

# wxnet (wimax network) simulation
#from sys import argv, stderr

import sys, os
sys.path.append(os.getcwd() + '/lib')

from core import *
from wxnet import *

def run(argv):

    sim = Sim('wxnet', argv)

    alg = WXBS.alg
    algS = size(alg)   
    
    if size(WXnet.tRate) > 1:
        lvName = 'WXnet.tRate'
    else:
        lvName = 'WLnet.nNod'
    # load variable name

    exec 'lv = %s' % lvName   # load variable
    
    lvS = size(lv) # load variable size
    # exec '%s = %s' % (var.split('.')[1], var)
    res = Result()
    res.init([algS,lvS])
    
    for k in range(algS):

        WXBS.alg = get(alg,k)  # defined in simlib,
     
        for n in range(lvS):
            dbs()
            exec '%s = int(get(lv,n))' % lvName
            _res = sim.run((k,n))
            if 's' in Sim.log:
                exec "sys.stderr.write('%s %s %.2f \n' % (Result.out,WXBS.alg,%s))" % lvName

            for key,value in res.val.items():
                for i in range(2):
                    res.val[key][k,n,i] = mean(_res.val[key][:,i])


    for key in res.val.keys():
        res.val[key] = res.val[key].squeeze()

    res.write(sys.argv)
    return res.val


if __name__ == '__main__':
    run(sys.argv)
        
    
         
    
    
        
